package goutil

import "unsafe"

//ref doc https://jaycechant.info/2019/golang-unsafe-cast-between-string-and-bytes/
//ref doc backup http://note.youdao.com/noteshare?id=60683a87632a4ee128df234fe66736c0

// StrToBytesUnsafe converts string to []byte without memory copy.
// 原则上不要修改转换后的[]byte,只作为一般常量使用
func StrToBytesUnsafe(s string) []byte {
	return *(*[]byte)(unsafe.Pointer(&s))
}

// BytesToStrUnsafe converts []byte to string without memory copy.
// 请注意，修改[]byte会直接修改string底层数据
func BytesToStrUnsafe(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}
