package crudhook

//sys -> TCrudHookSys
//go:generate syncmap -name TCrudHookSysMap -pkg crudhook "map[string]*TCrudHookSys"

//crud operation -> hook list
//go:generate syncmap -name TCrudHookHandlerMap -pkg crudhook "map[string]*TCrudHookList"

//go:generate syncmap -name TCrudHookList -pkg crudhook "map[*TfnCrudHook]bool"

//crud hook, if before hook return err,crud action will not exec
type TfnCrudHook func(sys string, userrid string, userorgrid string, operation string, reqProto interface{}, reqOptionProto interface{}, ipinfo string) error

type TCrudHookSys struct {
	Sys         string
	BeforeHooks *TCrudHookHandlerMap
	AfterHooks  *TCrudHookHandlerMap
}
type TCrudHookManager struct {
	SysHooks *TCrudHookSysMap
}

//全局crud hook manager
var GlobalCrudHook = New()

func New() TCrudHookManager {
	return TCrudHookManager{SysHooks: &TCrudHookSysMap{}}
}

//operation = "all"为hook所有operation
func (this *TCrudHookManager) AddCrudBeforeHook(sys string, operation string, handler TfnCrudHook) {
	hookSys, exist := this.SysHooks.Load(sys)

	if exist {
		hookList, ok := hookSys.BeforeHooks.Load(operation)
		if ok {
			hookList.Store(&handler, true)
		} else {
			hookList = &TCrudHookList{}
			hookList.Store(&handler, true)
			hookSys.BeforeHooks.Store(operation, hookList)
		}
	} else {
		hookSys = &TCrudHookSys{
			Sys:         sys,
			BeforeHooks: &TCrudHookHandlerMap{},
			AfterHooks:  &TCrudHookHandlerMap{},
		}
		hookList := &TCrudHookList{}
		hookList.Store(&handler, true)
		hookSys.BeforeHooks.Store(operation, hookList)
		this.SysHooks.Store(sys, hookSys)

	}
}
func (this *TCrudHookManager) DeleteCrudBeforeHook(sys string, operation string, handler TfnCrudHook) {
	hookSys, exist := this.SysHooks.Load(sys)

	if exist {
		hookList, ok := hookSys.BeforeHooks.Load(operation)
		if ok {
			hookList.Delete(&handler)
		}
	}
}

//operation = "all"为hook所有operation
func (this *TCrudHookManager) AddCrudAfterHook(sys string, operation string, handler TfnCrudHook) {
	hookSys, exist := this.SysHooks.Load(sys)

	if exist {
		hookList, ok := hookSys.AfterHooks.Load(operation)
		if ok {
			hookList.Store(&handler, true)
		} else {
			hookList = &TCrudHookList{}
			hookList.Store(&handler, true)
			hookSys.AfterHooks.Store(operation, hookList)
		}
	} else {
		hookSys = &TCrudHookSys{
			Sys:         sys,
			BeforeHooks: &TCrudHookHandlerMap{},
			AfterHooks:  &TCrudHookHandlerMap{},
		}
		hookList := &TCrudHookList{}
		hookList.Store(&handler, true)
		hookSys.AfterHooks.Store(operation, hookList)
		this.SysHooks.Store(sys, hookSys)

	}

}

func (this *TCrudHookManager) DeleteCrudAfterHook(sys string, operation string, handler TfnCrudHook) {
	hookSys, exist := this.SysHooks.Load(sys)

	if exist {
		hookList, ok := hookSys.AfterHooks.Load(operation)
		if ok {
			hookList.Delete(&handler)
		}
	}
}

func CrudBeforeHook(sys string, userrid string, userorgrid string, operation string, reqProto interface{}, reqOptionProto interface{}, ipinfo string) error {
	hooksys, ok := GlobalCrudHook.SysHooks.Load(sys)
	if !ok {
		return nil
	}
	{
		//for all hook
		hooklist, ok := hooksys.BeforeHooks.Load("all")
		if ok {
			var err error

			hooklist.Range(func(handler *TfnCrudHook, value bool) bool {
				err = (*handler)(sys, userrid, userorgrid, operation, reqProto, reqOptionProto, ipinfo)
				if err != nil {
					return false
				}
				return true
			})
			if err != nil {
				return err
			}
		}

	}
	hooklist, ok := hooksys.BeforeHooks.Load(operation)
	if !ok {
		return nil
	}
	var err error

	hooklist.Range(func(handler *TfnCrudHook, value bool) bool {
		err = (*handler)(sys, userrid, userorgrid, operation, reqProto, reqOptionProto, ipinfo)
		if err != nil {
			return false
		}
		return true
	})

	return err

}

func CrudAfterHook(sys string, userrid string, userorgrid string, operation string, reqProto interface{}, reqOptionProto interface{}, ipinfo string) {
	hooksys, ok := GlobalCrudHook.SysHooks.Load(sys)
	if !ok {
		return
	}
	{
		hooklist, ok := hooksys.AfterHooks.Load("all")
		if ok {
			hooklist.Range(func(handler *TfnCrudHook, value bool) bool {
				_ = (*handler)(sys, userrid, userorgrid, operation, reqProto, reqOptionProto, ipinfo)
				return true
			})
		}

	}
	hooklist, ok := hooksys.AfterHooks.Load(operation)
	if !ok {
		return
	}

	hooklist.Range(func(handler *TfnCrudHook, value bool) bool {
		_ = (*handler)(sys, userrid, userorgrid, operation, reqProto, reqOptionProto, ipinfo)
		return true
	})

}
