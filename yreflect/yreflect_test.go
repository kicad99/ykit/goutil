package yreflect

import (
	"reflect"
	"testing"
)

type MscNetConfig struct {
	MscId   uint32 `protobuf:"varint,1,opt,name=msc_id,json=mscId,proto3" json:"msc_id,omitempty"`
	MscPort uint32 `protobuf:"varint,2,opt,name=msc_port,json=mscPort,proto3" json:"msc_port,omitempty"`
}

func reverseMap(m map[string]string) map[string]string {
	n := make(map[string]string, len(m))
	for k, v := range m {
		n[v] = k
	}
	return n
}
func TestGetStructAllFieldNamesAndJsonTag(t *testing.T) {
	type args struct {
		obj            interface{}
		deep           bool
		fieldnamefirst bool
	}
	tests := []struct {
		name    string
		args    args
		want    map[string]string
		wantErr bool
	}{
		{name: "test-fieldnamefirst", args: args{obj: &MscNetConfig{}, deep: true, fieldnamefirst: true}, want: map[string]string{"MscId": "msc_id", "MscPort": "msc_port"}, wantErr: false},
		{name: "test-tagnamefirst", args: args{obj: &MscNetConfig{}, deep: true, fieldnamefirst: false}, want: reverseMap(map[string]string{"MscId": "msc_id", "MscPort": "msc_port"}), wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetStructAllFieldNamesAndJsonTag(tt.args.obj, tt.args.deep, tt.args.fieldnamefirst)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetStructAllFieldNamesAndJsonTag() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetStructAllFieldNamesAndJsonTag() got = %v, want %v", got, tt.want)
			}
		})
	}
}
