package goutil

type IMarshaler interface {
	Marshal() (dAtA []byte, err error)
}
