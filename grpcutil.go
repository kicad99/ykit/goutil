package goutil

import (
	"context"
	"google.golang.org/grpc/metadata"
)

func GetIpinfoFromMeta(grpcctx context.Context) string {
	md, ok := metadata.FromIncomingContext(grpcctx)
	if !ok {
		return ""
	}
	ipinfo := md.Get("ykit-ipinfo")
	if len(ipinfo) > 0 {
		return ipinfo[0]
	}

	return ""
}

func GetUserOrgRIDFromMeta(grpcctx context.Context) string {
	md, ok := metadata.FromIncomingContext(grpcctx)
	if !ok {
		return ""
	}
	orgrid := md.Get("ykit-userorgrid")
	if len(orgrid) > 0 {
		return orgrid[0]
	}

	return ""
}
