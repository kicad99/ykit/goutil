package goutil

import "strings"

//go:generate syncmap -name TstrMap -pkg goutil map[string]string

//search substr in s, search start pos at offset
func StrIndex(s, substr string, offset int) int {
	if len(s) < offset {
		return -1
	}
	if idx := strings.Index(s[offset:], substr); idx >= 0 {
		return offset + idx
	}
	return -1
}

//在无序的列表s中找substr,找不到返回-1
//如果s是有序的，请使用sort.SearchStrings
func StrSliceIndex(s []string, substr string) int {
	return StrSliceIndexOffset(s, substr, 0)
}

//在无序的列表s中找substr,从offset开始,找不到返回-1
//如果s是有序的，请使用sort.SearchStrings
func StrSliceIndexOffset(s []string, substr string, offset int) int {
	sLen := len(s)
	if sLen < offset {
		return -1
	}

	for ; offset < sLen; offset++ {
		if s[offset] == substr {
			return offset
		}
	}

	return -1
}

//delete str in str slice, do not keep order
func StrSliceDelete(strs []string, delStr string) (result []string, deleted bool) {
	for i, str := range strs {
		if str == delStr {
			strs[i] = strs[len(strs)-1]
			strs = strs[:len(strs)-1]
			return strs, true
		}
	}

	return strs, false
}

//delete str in str slice, keep original order
func StrSliceDeleteOrder(strs []string, delStr string) (result []string, deleted bool) {
	for i, str := range strs {
		if str == delStr {
			return strs[:i+copy(strs[i:], strs[i+1:])], true
		}
	}

	return strs, false
}

// Cut cuts s around the first instance of sep,
// returning the text before and after sep.
// The found result reports whether sep appears in s.
// If sep does not appear in s, cut returns s, "", false.
func Cut(s, sep string) (before, after string, found bool) {
	if i := strings.Index(s, sep); i >= 0 {
		return s[:i], s[i+len(sep):], true
	}
	return s, "", false
}
