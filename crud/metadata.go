package crud

import (
	"context"
	"encoding/base64"

	"gitlab.com/kicad99/ykit/goutil/rpc"
	"google.golang.org/grpc/metadata"
)

func GetDMLParamFromRpcMetadata(ctx context.Context) (dmrParam *DMLParam, err error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, rpc.ENoMetaDataFound
	}
	DMLParamMeta := md.Get("DMLParam")
	if len(DMLParamMeta) < 1 {
		return nil, rpc.EnoDMLParamFound
	}
	b64DMLParam := DMLParamMeta[0]
	binDMLParam, err := base64.StdEncoding.DecodeString(b64DMLParam)
	if err != nil {
		return nil, err
	}

	dmrParam = &DMLParam{}
	err = dmrParam.Unmarshal(binDMLParam)
	if err != nil {
		return nil, err
	}

	return dmrParam, nil
}
