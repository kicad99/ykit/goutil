package crud

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"reflect"
	"strconv"
	"strings"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/kicad99/ykit/goutil/yreflect"
)

var ColsIsNotEqualToVals = errors.New("columns is not equal to vals")
var NoColsOrVals = errors.New("no columns or no vals")
var NoColumnFieldsInObj = errors.New("no column field in obj")
var NoRIDColumnFieldInObj = errors.New("no RID column in table")
var EBadOrderByClause = errors.New("bad order by clause")

var nullString *string

//得到obj 对象的struct type名
func StructType(obj interface{}) string {
	t := reflect.TypeOf(obj).String()
	dotpos := strings.LastIndex(t, ".")
	if dotpos < 0 {
		return t
	} else {
		return t[dotpos+1:]
	}
}

//得到rows的所有列名
func RowsColNames(rows pgx.Rows) []string {
	rowDesc := rows.FieldDescriptions()
	cols := make([]string, len(rowDesc))
	for i, desc := range rowDesc {
		cols[i] = string(desc.Name)
	}

	return cols
}

//把数据库的字段名转换为obj struct FieldName,如果无法转换，保留数据库原名
func RowsColNames2GoFieldNames(rows pgx.Rows, obj interface{}) []string {
	objFields := yreflect.GetStructAllFieldNames(obj)
	objFieldMap := make(map[string]string, 0)
	for _, fieldname := range objFields {
		lowerFieldName := strings.ToLower(fieldname)
		objFieldMap[lowerFieldName] = fieldname
	}

	rowDesc := rows.FieldDescriptions()
	cols := make([]string, len(rowDesc))
	for i, desc := range rowDesc {
		cols[i] = string(desc.Name)
		objField, exists := objFieldMap[cols[i]]
		if exists {
			cols[i] = objField
		}
	}

	return cols
}

//取得结构体的fieldName和ykit Tag
func YkitStructAllFieldAndTag(obj interface{}) map[string]string {
	objValue := reflect.ValueOf(obj).Elem()
	objType := objValue.Type()
	fieldsCount := objType.NumField()

	r := make(map[string]string, fieldsCount)

	for i := 0; i < fieldsCount; i++ {
		field := objType.Field(i)
		if yreflect.IsExportableField(field) {
			fieldTag := field.Tag.Get("ykit")
			r[field.Name] = fieldTag
		}
	}

	return r
}

//得到插入数据库表用到的所有列名
func YkitColumnInsertFields(obj interface{}) []string {
	fieldMap := YkitStructAllFieldAndTag(obj)
	var allFields []string

	for fieldName, fieldTag := range fieldMap {
		if strings.Contains(fieldTag, "nodb") || strings.Contains(fieldTag, "generated") {
			continue
		}
		allFields = append(allFields, fieldName)
	}

	return allFields
}

//此字段在Proto定义的数据库表中是否是唯一字段
func IsFieldDbUnique(obj interface{}, fieldName string) bool {
	ykitTag, _ := yreflect.GetFieldTag(obj, fieldName, "ykit")

	return strings.Contains(ykitTag, "prk") || strings.Contains(ykitTag, "unique")
}

//以obj struct type为表名，obj 中各个field值为sql相应字段值，执行insert sql操作
func YkitDbInsertReflect(obj interface{}, db *pgxpool.Conn) (insertRow int32, err error) {
	tableName := StructType(obj)
	return YkitDbInsertReflectWithTableName(obj, tableName, db)
}

//以table为表名，obj 中各个field值为sql相应字段值，执行insert sql操作
func YkitDbInsertReflectWithTableName(obj interface{}, table string, db *pgxpool.Conn) (insertRow int32, err error) {
	ColumnNameAndTags := YkitStructAllFieldAndTag(obj)

	if len(ColumnNameAndTags) == 0 {
		return 0, NoColumnFieldsInObj
	}

	vals := make([]interface{}, 0, len(ColumnNameAndTags))
	ColumnNames := make([]string, 0, len(ColumnNameAndTags))
	for col, tag := range ColumnNameAndTags {
		fieldVal, err := yreflect.GetField(obj, col)
		if err != nil {
			return 0, err
		}
		if strings.Contains(tag, "null") && !strings.Contains(tag, "notnull") {
			str, isstring := fieldVal.(string)
			if isstring {
				if len(str) == 0 {
					fieldVal = nullString
				}
			}
		}
		vals = append(vals, fieldVal)
		ColumnNames = append(ColumnNames, col)
	}

	return DbInsert(table, ColumnNames, vals, db)
}

//插入数据到表table中
//insert into table (columnNames0,columnNames1,...) values(vals0,vals1,...)
func DbInsert(table string, columnNames []string, vals []interface{}, db *pgxpool.Conn) (insertRow int32, err error) {
	if len(columnNames) == 0 || len(vals) == 0 {
		return 0, NoColsOrVals
	}
	insertSql := strings.Builder{}
	insertSql.WriteString("insert into ")
	insertSql.WriteString(table)
	insertSql.WriteString(" (")

	for i, col := range columnNames {
		if i != 0 {
			insertSql.WriteString(",")
		}
		insertSql.WriteString(col)
	}
	insertSql.WriteString(") values(")
	for j := range columnNames {
		if j != 0 {
			insertSql.WriteString(",")
		}
		insertSql.WriteString("$")
		insertSql.WriteString(strconv.Itoa(j + 1))
	}

	insertSql.WriteString(");")

	cmdTag, err := db.Exec(context.Background(), insertSql.String(), vals...)
	if cmdTag != nil {
		return int32(cmdTag.RowsAffected()), err

	} else {
		return 0, err
	}
}

//以obj struct type为表名，obj 中各个field值为sql相应字段值,执行sql update操作
//要求表primary key字段名为RID
func YkitDbUpdateReflect(obj interface{}, db *pgxpool.Conn) (updatedRow int32, err error) {
	tableName := StructType(obj)
	return YkitDbUpdateReflectWithTableName(obj, tableName, db)
}

//以table为表名，obj 中各个field值为sql相应字段值,执行sql update操作
//要求表primary key字段名为RID
func YkitDbUpdateReflectWithTableName(obj interface{}, table string, db *pgxpool.Conn) (updatedRow int32, err error) {
	ColumnNameAndTags := YkitStructAllFieldAndTag(obj)
	hasRID := false
	ColumnNames := make([]string, 0, len(ColumnNameAndTags))
	vals := make([]interface{}, 0, len(ColumnNameAndTags))

	for col, tag := range ColumnNameAndTags {
		if col == "RID" {
			hasRID = true
			continue
		}

		fieldVal, err := yreflect.GetField(obj, col)
		if err != nil {
			return 0, err
		}

		if strings.Contains(tag, "null") && !strings.Contains(tag, "notnull") {
			str, isstring := fieldVal.(string)
			if isstring {
				if len(str) == 0 {
					fieldVal = nullString
				}
			}
		}
		vals = append(vals, fieldVal)
		ColumnNames = append(ColumnNames, col)
	}

	if len(ColumnNames) == 0 {
		return 0, NoColumnFieldsInObj
	}
	if !hasRID {
		return 0, NoRIDColumnFieldInObj
	}

	RIDVal, err := yreflect.GetField(obj, "RID")
	if err != nil {
		return 0, err
	}

	updated, err := DbUpdate(table, "RID", RIDVal, ColumnNames, vals, db)

	return int32(updated), err
}

//以obj struct type为表名，obj 中updateColumns指定的各个field值为sql相应字段值,执行sql update操作
//要求表primary key字段名为RID
func YkitDbPartialUpdateReflect(obj interface{}, updateColumns []string, db *pgxpool.Conn) (updatedRow int32, err error) {
	if len(updateColumns) == 0 {
		return 0, NoColsOrVals
	}
	tableName := StructType(obj)
	return YkitDbPartialUpdateReflectWithTableName(obj, tableName, updateColumns, db)
}

//以table为表名，obj 中updateColumns各个field值为sql相应字段值,执行sql update操作
//要求表primary key字段名为RID
func YkitDbPartialUpdateReflectWithTableName(obj interface{}, table string, updateColumns []string, db *pgxpool.Conn) (updatedRow int32, err error) {
	if len(updateColumns) == 0 {
		return 0, NoColsOrVals
	}

	vals := make([]interface{}, 0, len(updateColumns))

	for _, col := range updateColumns {
		fieldVal, err := yreflect.GetField(obj, col)
		if err != nil {
			return 0, err
		}
		str, isstring := fieldVal.(string)
		if isstring {
			if len(str) == 0 {
				ykitTag, _ := yreflect.GetFieldTag(obj, col, "ykit")
				if strings.Contains(ykitTag, "null") && !strings.Contains(ykitTag, "notnull") {
					fieldVal = nullString
				}
			}
		}
		vals = append(vals, fieldVal)
	}

	RIDVal, err := yreflect.GetField(obj, "RID")
	if err != nil {
		return 0, err
	}

	updated, err := DbUpdate(table, "RID", RIDVal, updateColumns, vals, db)

	return int32(updated), err
}

//使用一个字段值更新表table里面的数据
//update table set (updateColumns0,updateColumns1,...)=row(updateVals0,updateVals1,...) where keyColumn=keyVal
func DbUpdate(table string, keyColumn string, keyVal interface{}, updateColumns []string, updateVals []interface{}, db *pgxpool.Conn) (updatedRow int64, err error) {
	_, err = IsFieldNameSecurity(keyColumn)
	if err != nil {
		return 0, err
	}
	_, err = IsFieldNamesSecurity(updateColumns)
	if err != nil {
		return 0, err
	}

	updateSql := strings.Builder{}
	updateSql.WriteString("update ")
	updateSql.WriteString(table)

	updateSql.WriteString(" set (")

	for i, col := range updateColumns {
		if i != 0 {
			updateSql.WriteString(",")
		}
		updateSql.WriteString(col)
	}

	updateSql.WriteString(")=ROW(")
	for j := range updateColumns {
		if j != 0 {
			updateSql.WriteString(",")
		}
		updateSql.WriteString("$")
		updateSql.WriteString(strconv.Itoa(j + 1))
	}

	updateSql.WriteString(") where ")
	updateSql.WriteString(keyColumn)
	updateSql.WriteString(" = $")

	vals := append(updateVals, keyVal)

	updateSql.WriteString(strconv.Itoa(len(vals)))
	updateSql.WriteString(";")

	cmdTag, err := db.Exec(context.Background(), updateSql.String(), vals...)
	if cmdTag != nil {
		return (cmdTag.RowsAffected()), err

	} else {
		return 0, err
	}
}

//使用多个字段值更新表table里面的数据
//update table set (updateColumns0,updateColumns1,...)=row(updateVals0,updateVals1,...) where keyColumn0=keyVal0 and keyColumn1=keyVal1 and ...
func DbUpdates(table string, keyColumns []string, keyVals []interface{}, updateColumns []string, updateVals []interface{}, db *pgxpool.Conn) (updatedRow int64, err error) {
	_, err = IsFieldNamesSecurity(keyColumns)
	if err != nil {
		return 0, err
	}
	_, err = IsFieldNamesSecurity(updateColumns)
	if err != nil {
		return 0, err
	}

	updateSql := strings.Builder{}
	updateSql.WriteString("update ")
	updateSql.WriteString(table)

	updateSql.WriteString(" set (")

	for i, col := range updateColumns {
		if i != 0 {
			updateSql.WriteString(",")
		}
		updateSql.WriteString(col)
	}

	updateSql.WriteString(")=ROW(")
	for j := range updateColumns {
		if j != 0 {
			updateSql.WriteString(",")
		}
		updateSql.WriteString("$")
		updateSql.WriteString(strconv.Itoa(j + 1))
	}

	updateSql.WriteString(") where ")
	for i, col := range keyColumns {
		if i != 0 {
			updateSql.WriteString(" and ")
		}
		updateSql.WriteString(col)
		updateSql.WriteString(" =$")
		updateSql.WriteString(strconv.Itoa(len(updateColumns) + i + 1))
	}

	vals := append(updateVals, keyVals...)

	updateSql.WriteString(";")

	cmdTag, err := db.Exec(context.Background(), updateSql.String(), vals...)
	if cmdTag != nil {
		return (cmdTag.RowsAffected()), err

	} else {
		return 0, err
	}
}

//以obj struct type为表名，obj 中RID值为sql相应字段值,执行sql delete操作
//要求表primary key字段名为RID
func YkitDbDeleteReflect(obj interface{}, db *pgxpool.Conn) (deletedRow int32, err error) {
	tableName := StructType(obj)
	return YkitDbDeleteReflectWithTableName(obj, tableName, db)
}

//以table为表名，obj 中RID值为sql相应字段值,执行sql delete操作
//要求表primary key字段名为RID
func YkitDbDeleteReflectWithTableName(obj interface{}, table string, db *pgxpool.Conn) (deletedRow int32, err error) {
	RIDfieldVal, err := yreflect.GetField(obj, "RID")
	if err != nil {
		return 0, err
	}

	deleted, err := DbDelete(table, "RID", RIDfieldVal, db)
	return int32(deleted), err
}

//使用一个值删除表里面的数据,
//delete from table where keyColumn=kevVal
func DbDelete(table string, keyColumn string, keyVal interface{}, db *pgxpool.Conn) (deleted int64, err error) {
	_, err = IsFieldNameSecurity(keyColumn)
	if err != nil {
		return 0, err
	}
	delSql := "delete from " + table + " where " + keyColumn + "=$1;"
	cmdTag, err := db.Exec(context.Background(), delSql, keyVal)
	if cmdTag != nil {
		return (cmdTag.RowsAffected()), err

	} else {
		return 0, err
	}
}

//使用多个值删除表里面的数据
//delete from table where keyColumns0=keyVals0 and keyColumns1=keyVals1 and ...
func DbDeletes(table string, keyColumns []string, keyVals []interface{}, db *pgxpool.Conn) (deleted int64, err error) {
	if len(keyColumns) != len(keyVals) {
		return 0, ColsIsNotEqualToVals
	}
	if len(keyColumns) == 0 {
		return 0, NoColsOrVals
	}
	_, err = IsFieldNamesSecurity(keyColumns)
	if err != nil {
		return 0, err
	}
	delSql := strings.Builder{}
	delSql.WriteString("delete from ")
	delSql.WriteString(table)
	delSql.WriteString(" where ")

	for i, col := range keyColumns {
		if i != 0 {
			delSql.WriteString(" and ")
		}
		delSql.WriteString(col)
		delSql.WriteString(" =$")
		delSql.WriteString(strconv.Itoa(i + 1))
	}
	delSql.WriteString(";")

	cmdTag, err := db.Exec(context.Background(), delSql.String(), keyVals)
	if cmdTag != nil {
		return (cmdTag.RowsAffected()), err

	} else {
		return 0, err
	}
}

//以obj struct type为表名，使用rid获取一行数据，返回值得填写到Obj中,resultColumns为空时，取此表所有的值
//select (resultColumns0,resultColumns1,... | * ) from objType where RID = obj.RID
func YkitDbSelectOneByRID(obj interface{}, resultColumns []string, db *pgxpool.Conn) (selectRow int32, err error) {
	keyColumns := []string{"RID"}
	return YkitDbSelectOne(obj, resultColumns, keyColumns, db)
}

//以obj struct type为表名，使用rids获取数据，返回值在rows中,resultColumns为空时，取此表所有的值
//select (resultColumns0,resultColumns1,... | * ) from objType where RID in RIDs
func YkitDbSelectByRIDs(obj interface{}, resultColumns []string, RIDs []string, db *pgxpool.Conn) (rows pgx.Rows, err error) {
	tableName := StructType(obj)

	return YkitDbSelectByRIDsWithTableName(tableName, resultColumns, RIDs, db)
}

//根据结果列，生成sql 字符串,空默认为取所有字段
func ResultCols2SQLString(resultCols []string, timeColumn []string) string {
	colSQL := strings.Builder{}
	if len(resultCols) > 0 {
		for i, col := range resultCols {
			if i != 0 {
				colSQL.WriteString(",")
			}
			hasGen := false
			for j := 1; j < len(timeColumn); j = j + 2 {
				if timeColumn[j-1] == col {
					timeMinutes, err := strconv.Atoi(timeColumn[j])
					if err != nil {
						break
					}

					colSQL.WriteString(col)

					if timeMinutes >= 0 {
						colSQL.WriteString(" + interval '" + timeColumn[j] + " minutes ' as ")

					} else {
						colSQL.WriteString(" - interval '" + strconv.Itoa(-timeMinutes) + " minutes ' as ")
					}
					colSQL.WriteString(col)

					hasGen = true
					break
				}
			}
			if !hasGen {
				colSQL.WriteString(col)
			}
		}
	} else {
		colSQL.WriteString(" * ")
	}
	return colSQL.String()
}

//以table为表名，使用rids获取数据，返回值在rows中,resultColumns为空时，取此表所有的值
//select (resultColumns0,resultColumns1,... | * ) from table where RID in RIDs
func YkitDbSelectByRIDsWithTableName(table string, resultColumns []string, RIDs []string, db *pgxpool.Conn) (rows pgx.Rows, err error) {
	_, err = IsFieldNamesSecurity(resultColumns)
	if err != nil {
		return nil, err
	}

	selectSql := strings.Builder{}
	selectSql.WriteString("select ")
	selectSql.WriteString(ResultCols2SQLString(resultColumns, nil))
	selectSql.WriteString(" from ")
	selectSql.WriteString(table)

	selectSql.WriteString(" where RID = any($1);")

	return db.Query(context.Background(), selectSql.String(), RIDs)
}

//以obj struct type为表名，使用keyColumns为条件获取一行数据，返回值得填写到Obj中,resultColumns为空时，取此表所有的值
//select (resultColumns0,resultColumns1,... | * ) from objType where keyColumns0=obj.keyColumns0 and ...
func YkitDbSelectOne(obj interface{}, resultColumns []string, keyColumns []string, db *pgxpool.Conn) (selectRow int32, err error) {
	rows, err := YkitDbSelect(obj, resultColumns, keyColumns, db)
	if rows != nil {
		defer rows.Close()
	}
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		columnNames := RowsColNames2GoFieldNames(rows, obj)
		vals, err := rows.Values()
		if err != nil {
			return 0, err
		}
		for i, col := range columnNames {
			err = yreflect.SetField(obj, col, vals[i])
			if err != nil {
				log.Println("ydb YkitDbSelectOne SetField err:", col, err)
			}
		}

		//lint:ignore SA4004 we want one row anyway
		return 1, nil
	}

	return 0, sql.ErrNoRows

}

//以obj struct type为表名，使用keyColumns为条件获取数据，返回值在rows中,resultColumns为空时，取此表所有的值
//返回的rows用完后一定要close,这个是sql查询处理的标准
//select (resultColumns0,resultColumns1,... | * ) from objType where keyColumns0=obj.keyColumns0 and ...
func YkitDbSelect(obj interface{}, resultColumns []string, keyColumns []string, db *pgxpool.Conn) (rows pgx.Rows, err error) {
	if len(keyColumns) == 0 {
		return nil, NoColsOrVals
	}

	tableName := StructType(obj)

	keyVals := make([]interface{}, 0)
	for _, col := range keyColumns {
		fieldVal, err := yreflect.GetField(obj, col)
		if err != nil {
			return nil, err
		}
		keyVals = append(keyVals, fieldVal)
	}

	return DbSelectSimple(tableName, resultColumns, keyColumns, keyVals, db)
}

//从表table中查询数据
//返回的rows用完后一定要close,这个是sql查询处理的标准
//select (resultColumns0,resultColumns1,... | * ) from table where keyCols0=keyVals0 and ...
func DbSelectSimple(table string, resultColumns []string, keyCols []string, keyVals []interface{}, db *pgxpool.Conn) (rows pgx.Rows, err error) {
	_, err = IsFieldNamesSecurity(keyCols)
	if err != nil {
		return nil, err
	}
	_, err = IsFieldNamesSecurity(resultColumns)
	if err != nil {
		return nil, err
	}
	selectSql := strings.Builder{}
	selectSql.WriteString("select ")
	selectSql.WriteString(ResultCols2SQLString(resultColumns, nil))
	selectSql.WriteString(" from ")
	selectSql.WriteString(table)

	if len(keyCols) > 0 {
		selectSql.WriteString(" where ")
	} else {
		selectSql.WriteString(" ")
	}

	for j, col := range keyCols {
		if j != 0 {
			selectSql.WriteString(" and ")
		}
		selectSql.WriteString(col)
		selectSql.WriteString("=$")
		selectSql.WriteString(strconv.Itoa(j + 1))
	}

	return db.Query(context.Background(), selectSql.String(), keyVals...)
}

//从表table中查询数据
//返回的rows用完后一定要close,这个是sql查询处理的标准
//select (resultColumns0,resultColumns1,... | * ) from table where where[0].field where[0].FieldCompareOperator where[0].val and ...
func DbSelectQuery(table string, queryPara *QueryParam, db *pgxpool.Conn) (rows pgx.Rows, err error) {
	if queryPara == nil {
		queryPara = &QueryParam{}
	}
	//检查列名安全
	for _, v := range queryPara.Where {
		_, err = IsFieldNameSecurity(v.Field)
		if err != nil {
			return nil, err
		}
	}

	_, err = IsFieldNamesSecurity(queryPara.ResultColumn)
	if err != nil {
		return nil, err
	}
	selectSql := strings.Builder{}
	selectSql.WriteString("select ")
	selectSql.WriteString(ResultCols2SQLString(queryPara.ResultColumn, queryPara.TimeColumn))
	selectSql.WriteString(" from ")
	selectSql.WriteString(table)

	if len(queryPara.Where) > 0 {
		selectSql.WriteString(" where ")
	} else {
		selectSql.WriteString(" ")
	}

	keyVals := make([]interface{}, 0, len(queryPara.Where))

	for j, w := range queryPara.Where {
		if j != 0 {
			selectSql.WriteString(" and ")
		}
		selectSql.WriteString(w.Field)
		selectSql.WriteString(" ")
		selectSql.WriteString(SecuritySQLOperator(w.FieldCompareOperator))
		selectSql.WriteString(" $")
		selectSql.WriteString(strconv.Itoa(j + 1))
		keyVals = append(keyVals, w.FieldValue)
	}

	if len(queryPara.OrderBy) > 0 {
		selectSql.WriteString(" order by ")
		for i, v := range queryPara.OrderBy {
			colAscDesc := strings.Split(v, " ")
			if len(colAscDesc) != 2 {
				return nil, EBadOrderByClause
			}

			_, err = IsFieldNameSecurity(colAscDesc[0])
			if err != nil {
				return nil, err
			}

			ascDesc := strings.ToLower(colAscDesc[1])
			if ascDesc != "asc" && ascDesc != "desc" {
				return nil, EBadOrderByClause
			}

			selectSql.WriteString(colAscDesc[0])
			selectSql.WriteString(" ")
			selectSql.WriteString(ascDesc)
			if i != len(queryPara.OrderBy)-1 {
				selectSql.WriteString(",")
			}
		}
		if queryPara.Limit != 0 {
			selectSql.WriteString(" limit ")
			selectSql.WriteString(strconv.FormatUint(uint64(queryPara.Limit), 10))
		}
		selectSql.WriteString(" offset ")
		selectSql.WriteString(strconv.FormatUint(uint64(queryPara.Offset), 10))
	}

	return db.Query(context.Background(), selectSql.String(), keyVals...)
}

//从表table中查询有权限的数据
//返回的rows用完后一定要close,这个是sql查询处理的标准
//select (resultColumns0,resultColumns1,... | * ) from table where orgidCol in orgRIDs and where[0].field where[0].FieldCompareOperator where[0].val and ...
func DbSelectPrivilege(table string, orgidColName string, privilegeParm *PrivilegeParam, db *pgxpool.Conn) (rows pgx.Rows, err error) {
	queryPara := privilegeParm.QueryCondition
	if queryPara == nil {
		queryPara = &QueryParam{}
	}

	system := privilegeParm.System

	sessionid := privilegeParm.SessionID
	userid, exist, err := GetUserRIDinSession(system, sessionid, true, db)
	if err != nil {
		return nil, err
	}
	if !exist {
		return nil, ENoSuchSessionFound
	}

	orgRIDs, err := GetUserPrivilegeOrgs(userid, system, true, db)
	if err != nil {
		return nil, err
	}

	return TableSelectColInPrivilege(table, []string{orgidColName}, [][]string{orgRIDs}, userid, db, queryPara)
}

//从表table中查询有权限的数据
//返回的rows用完后一定要close,这个是sql查询处理的标准
//select (resultColumns0,resultColumns1,... | * ) from table where ColInName[i] =any (ColInVal[i]) and where[0].field where[0].FieldCompareOperator where[0].val and ...
func DbSelectColInPrivilege(table string, ColInName []string, ColInVal [][]string, privilegeParm *PrivilegeParam, db *pgxpool.Conn) (rows pgx.Rows, err error) {
	queryPara := privilegeParm.QueryCondition
	if queryPara == nil {
		queryPara = &QueryParam{}
	}

	for len(ColInName) > len(ColInVal) {
		ColInVal = append(ColInVal, []string{})
	}

	system := privilegeParm.System

	sessionid := privilegeParm.SessionID
	userid, exist, err := GetUserRIDinSession(system, sessionid, true, db)
	if err != nil {
		return nil, err
	}
	if !exist {
		return nil, ENoSuchSessionFound
	}
	return TableSelectColInPrivilege(table, ColInName, ColInVal, userid, db, queryPara)
}

//TableSelectColInPrivilege 类似 DbSelectColInPrivilege，只有少了部分代码
func TableSelectColInPrivilege(table string, ColInName []string, ColInVal [][]string, userid string, db *pgxpool.Conn, queryPara *QueryParam) (pgx.Rows, error) {
	//check has query permission
	hasperm, err := IsUserHasPermissionDB(userid, "db", table+".Query", db)
	if err != nil {
		return nil, err
	}
	if !hasperm {
		return nil, EnoPermission
	}

	//检查列名安全
	for _, v := range queryPara.Where {
		_, err = IsFieldNameSecurity(v.Field)
		if err != nil {
			return nil, err
		}
	}

	_, err = IsFieldNamesSecurity(queryPara.ResultColumn)
	if err != nil {
		return nil, err
	}
	selectSql := strings.Builder{}
	selectSql.WriteString("select ")
	selectSql.WriteString(ResultCols2SQLString(queryPara.ResultColumn, queryPara.TimeColumn))
	selectSql.WriteString(" from ")
	selectSql.WriteString(table)
	selectSql.WriteString(" where ")

	keyVals := make([]interface{}, 0, len(queryPara.Where)+len(ColInName))

	isFirstCond := true

	for i, colname := range ColInName {
		_, err = IsFieldNameSecurity(colname)
		if err != nil {
			return nil, err
		}
		if isFirstCond {
			isFirstCond = false
		} else {
			selectSql.WriteString(" and ")
		}

		selectSql.WriteString(colname)
		selectSql.WriteString(" = any ($")
		keyVals = append(keyVals, ColInVal[i])
		selectSql.WriteString(strconv.Itoa(len(keyVals)))
		selectSql.WriteString(")")
	}

	for _, w := range queryPara.Where {
		if isFirstCond {
			isFirstCond = false
		} else {
			selectSql.WriteString(" and ")
		}
		selectSql.WriteString(w.Field)
		selectSql.WriteString(" ")
		selectSql.WriteString(SecuritySQLOperator(w.FieldCompareOperator))
		selectSql.WriteString(" $")
		keyVals = append(keyVals, w.FieldValue)
		selectSql.WriteString(strconv.Itoa(len(keyVals)))
	}

	if len(queryPara.OrderBy) > 0 {
		selectSql.WriteString(" order by ")
		for i, v := range queryPara.OrderBy {
			colAscDesc := strings.Split(v, " ")
			if len(colAscDesc) != 2 {
				return nil, EBadOrderByClause
			}

			_, err = IsFieldNameSecurity(colAscDesc[0])
			if err != nil {
				return nil, err
			}

			ascDesc := strings.ToLower(colAscDesc[1])
			if ascDesc != "asc" && ascDesc != "desc" {
				return nil, EBadOrderByClause
			}

			selectSql.WriteString(colAscDesc[0])
			selectSql.WriteString(" ")
			selectSql.WriteString(ascDesc)
			if i != len(queryPara.OrderBy)-1 {
				selectSql.WriteString(",")
			}
		}
		if queryPara.Limit != 0 {
			selectSql.WriteString(" limit ")
			selectSql.WriteString(strconv.FormatUint(uint64(queryPara.Limit), 10))
		}
		selectSql.WriteString(" offset ")
		selectSql.WriteString(strconv.FormatUint(uint64(queryPara.Offset), 10))
	}

	return db.Query(context.Background(), selectSql.String(), keyVals...)
}
