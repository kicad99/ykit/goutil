package crud

import (
	"errors"
	"strings"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

var EFieldNameContainSpace = errors.New("field name contains space")
var EFieldNameContainComma = errors.New("field name contains ,")
var EFieldNameContainComment = errors.New("field name contains --")
var EFieldNameContainSemicolon = errors.New("field name contains ;")
var ErrLackOrgPrivilege = errors.New("lack org privilege")

//是否是安全的字段名
func IsFieldNameSecurity(fieldname string) (ok bool, err error) {
	if strings.Contains(fieldname, " ") {
		return false, EFieldNameContainSpace
	}
	if strings.Contains(fieldname, ",") {
		return false, EFieldNameContainComma
	}
	if strings.Contains(fieldname, ";") {
		return false, EFieldNameContainSemicolon
	}
	if strings.Contains(fieldname, "--") {
		return false, EFieldNameContainComment
	}
	return true, nil
}

//是否是安全的字段名列表
func IsFieldNamesSecurity(fieldNames []string) (ok bool, err error) {
	for _, v := range fieldNames {
		ok, err = IsFieldNameSecurity(v)
		if !ok {
			return
		}
	}

	return true, nil
}

//sql 字段名转换为安全的形式，防止sql注入
func SecuritySQLField(filedName string) (r string) {
	r = strings.Replace(filedName, " ", "", -1)
	r = strings.Replace(r, ",", "", -1)
	r = strings.Replace(r, ";", "", -1)
	r = strings.Replace(r, "--", "", -1)
	return
}

//sql 操作符转换为安全的形式，防止sql注入
func SecuritySQLOperator(operator string) string {
	return SecuritySQLField(operator)
}

//CRUD insert
func Insert(obj interface{}, db *pgxpool.Conn) (r *DMLResult, err error) {
	r = &DMLResult{}

	insertedRow, err := YkitDbInsertReflect(obj, db)

	if err != nil {
		r.ErrInfo = err.Error()
	}

	r.AffectedRow = insertedRow

	return r, err
}

//CRUD update
func Update(obj interface{}, db *pgxpool.Conn) (r *DMLResult, err error) {
	r = &DMLResult{}

	updatedRow, err := YkitDbUpdateReflect(obj, db)

	if err != nil {
		r.ErrInfo = err.Error()
	}

	r.AffectedRow = updatedRow

	return r, err
}

//CRUD partial update
func PartialUpdate(obj interface{}, updateCols []string, db *pgxpool.Conn) (r *DMLResult, err error) {
	r = &DMLResult{}

	updatedRow, err := YkitDbPartialUpdateReflect(obj, updateCols, db)

	if err != nil {
		r.ErrInfo = err.Error()
	}

	r.AffectedRow = updatedRow

	return r, err
}

//crud delete
func Delete(obj interface{}, db *pgxpool.Conn) (r *DMLResult, err error) {
	r = &DMLResult{}

	deletedRow, err := YkitDbDeleteReflect(obj, db)

	if err != nil {
		r.ErrInfo = err.Error()
	}

	r.AffectedRow = deletedRow

	return r, err
}

//crud select one
func SelectOne(obj interface{}, resultCols []string, db *pgxpool.Conn) (selectRow int32, err error) {

	return YkitDbSelectOne(obj, resultCols, []string{"RID"}, db)
}

//crud select by rids
func SelectByRids(obj interface{}, resultCols []string, rids []string, db *pgxpool.Conn) (rows pgx.Rows, err error) {

	return YkitDbSelectByRIDs(obj, resultCols, rids, db)
}
