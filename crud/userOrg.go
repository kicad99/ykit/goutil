package crud

import (
	"context"
	"errors"
	"strings"
	"time"

	"github.com/allegro/bigcache"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/kicad99/ykit/goutil"
	"gitlab.com/kicad99/ykit/goutil/pgxdb"
)

//var EBadUserRID = errors.New("bad user rid")
var ErrBadOrgRID = errors.New("bad orgrid")

//usrrid -> orgrid stringlist, split by ,
var userOrgsCache *bigcache.BigCache

func init() {
	cache, err := bigcache.NewBigCache(bigcache.DefaultConfig(5 * time.Minute))
	if err != nil {
		panic(err)
	}
	userOrgsCache = cache
}

//删除所有用户的权限数据缓存，因为操作了dborg
func ResetUserOrgCache() {
	//println("reset before:", userOrgsCache.Len())
	//debug.PrintStack()
	_ = userOrgsCache.Reset()
	//println("reset after:", userOrgsCache.Len())

}

//获得缓存的用户有权限的群组数据
func GetUserOrgsCache(usrrid string) (orgRIDs []string, exist bool) {
	v, err := userOrgsCache.Get(usrrid)
	if err != nil {
		return nil, false
	}
	str := goutil.BytesToStrUnsafe(v)
	if len(str) < 5 {
		//uuid 不可能小于5的，这种情况是用户没有任何群组权限
		return nil, true
	}
	orgRIDs = strings.Split(str, ",")
	return orgRIDs, true
}

//用户有权限的群组数据加入缓存
func SetUserOrgsCache(userRID string, orgRIDs []string) {
	//debug.PrintStack()
	//println("SetUserOrgsCache", userRID, orgRIDs)
	v := strings.Join(orgRIDs, ",")
	_ = userOrgsCache.Set(userRID, []byte(v))
}

//删除用户有权限的群组数据缓存
func DelUserOrgsCache(userRID string) {
	_ = userOrgsCache.Delete(userRID)
}

//查询用户有权限的群组，返回orgrid 列表
//queryCache: 是否从内存的缓存里面查找
func GetUserPrivilegeOrgs(userRID string, system string, queryCache bool, db *pgxpool.Conn) (orgRIDs []string, err error) {
	if queryCache {
		//从缓存看有没有
		cacheOrgs, exist := GetUserOrgsCache(userRID)
		if exist {
			return cacheOrgs, nil
		}
	}

	if db == nil {
		db, err = pgxdb.GetDbConn(system)
		if err != nil {
			return nil, err
		}

		defer pgxdb.ReleaseDbConn(db)
	}

	return GetUserPrivilegeOrgsDB(userRID, db)
}

func GetUserPrivilegeOrgsFromCache(userRID string) (orgRIDs []string, exist bool) {
	return GetUserOrgsCache(userRID)
}

//查询用户有权限的群组，返回orgrid 列表
func GetUserPrivilegeOrgsDB(userRID string, db *pgxpool.Conn) (orgRIDs []string, err error) {
	sqlStr := `select myorg($1);`
	rows, err := db.Query(context.Background(), sqlStr, userRID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var orgrid string
		err = rows.Scan(&orgrid)
		if err != nil {
			return orgRIDs, err
		}
		orgRIDs = append(orgRIDs, orgrid)
	}
	//设置缓存
	SetUserOrgsCache(userRID, orgRIDs)
	return orgRIDs, nil
}

//查询在system内，userrid是否对orgRID有权限
func IsUserHasThisOrgPrivilege(system string, userRID string, orgRID string) (bool, error) {
	if len(orgRID) < 5 {
		return false, ErrBadOrgRID
	}
	if len(system) == 0 || len(userRID) == 0 {
		return false, nil
	}
	cacheOrgs, exists := GetUserOrgsCache(userRID)
	if exists {
		found := goutil.StrSliceIndex(cacheOrgs, orgRID)
		if found != -1 {
			return true, nil
		} else {
			return false, nil
		}
	}

	dbOrgs, err := GetUserPrivilegeOrgs(userRID, system, false, nil)
	if err != nil {
		return false, err
	}

	found := goutil.StrSliceIndex(dbOrgs, orgRID)
	if found != -1 {
		return true, nil
	} else {
		return false, nil
	}

}

//grpc call, check if has orgrid privilege
func GrpcCheckHasOrgridPrivilege(ctx context.Context, orgrid string) error {
	sys, userrid, _ := pgxdb.GetYkitInfoFromMetadata(ctx)
	hasPri, err := IsUserHasThisOrgPrivilege(sys, userrid, orgrid)
	if err != nil {
		return err
	}
	if !hasPri {
		return ErrLackOrgPrivilege
	}

	return nil
}
