package crud

import "strconv"

//Where2SQL 将where 数据转换为sql where 后面的sql 条件语句
func Where2SQL(where []*WhereItem) (sqlStr string) {
	if len(where) == 0 {
		return ""
	}
	for i, v := range where {
		if i != 0 {
			sqlStr += " and "
		}
		sqlStr += SecuritySQLField(v.Field) + " " + SecuritySQLOperator(v.FieldCompareOperator) + " " + v.FieldValue

	}

	return sqlStr
}

//返回带位置参数的sql和相应的位置参数值列表
func Where2SQLPosition(where []*WhereItem) (sqlStr string, vals []interface{}) {
	if len(where) == 0 {
		return "", nil
	}
	pos := 1
	for i, v := range where {
		if i != 0 {
			sqlStr += " and "
		}
		sqlStr += SecuritySQLField(v.Field) + " " + SecuritySQLOperator(v.FieldCompareOperator) + " $" + strconv.Itoa(pos)
		pos++
		vals = append(vals, v.FieldValue)

	}

	return sqlStr, vals
}
