package crud

import (
	"context"
	"database/sql"
	"errors"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/kicad99/ykit/goutil/pgxdb"
)

//EnoPermission 没有操作权限
var EnoPermission = errors.New("No operation permission")

//IsUserHasPermission 查询系统内用户是否有相应的操作权限
func IsUserHasPermission(sys, userRID, permType, permValue string) (bool, error) {
	db, err := pgxdb.GetDbConn(sys)
	if err != nil {
		return false, err
	}
	defer pgxdb.ReleaseDbConn(db)

	return IsUserHasPermissionDB(userRID, permType, permValue, db)
}

//IsUserHasPermission 检查是否有相应的操作权限
func IsUserHasPermissionDB(userRID string, permType string, permValue string, db *pgxpool.Conn) (bool, error) {
	sqlStr := `select  rid from dbpermission where 
rid in (select permissionrid from dbrolepermission where rolerid  in (select  rolerid from dbuserrole d where userrid = $1)) 
and permissiontype = $2 
and permissionvalue = $3; `

	var tmpRid string
	err := db.QueryRow(context.Background(), sqlStr, userRID, permType, permValue).Scan(&tmpRid)

	if err == pgx.ErrNoRows || err == sql.ErrNoRows {
		return false, EnoPermission
	}

	if err != nil {
		return false, err
	}

	return true, nil
}

//GrpcCheckHasPermission grpc检查是否有相应的操作权限
func GrpcCheckHasPermission(ctx context.Context, permType, permValue string) (bool, error) {
	sys, userrid, _ := pgxdb.GetYkitInfoFromMetadata(ctx)
	return IsUserHasPermission(sys, userrid, permType, permValue)
}

//查询用户有权限的rolerid 列表
func GetUserRoleRIDs(userRID string, db *pgxpool.Conn) (roleRIDs []string, err error) {
	sqlStr := "select rolerid from dbuserrole where userrid = $1;"
	rows, err := db.Query(context.Background(), sqlStr, userRID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var rolerid string
		err = rows.Scan(&rolerid)
		if err != nil {
			return roleRIDs, err
		}
		roleRIDs = append(roleRIDs, rolerid)
	}
	return roleRIDs, nil
}
