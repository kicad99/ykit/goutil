package crud

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"github.com/allegro/bigcache"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/kicad99/ykit/goutil/pgxdb"
)

var ENoSuchSessionFound = errors.New("no such session found")

//sessionid -> userrid
var userSessionCache *bigcache.BigCache

func init() {
	cache, err := bigcache.NewBigCache(bigcache.DefaultConfig(15 * time.Minute))
	if err != nil {
		panic(err)
	}
	userSessionCache = cache
}

//从sessionid获得用户rid
func GetUserRIDinSession(system string, sessionid string, queryCache bool, db *pgxpool.Conn) (rid string, exist bool, err error) {
	if queryCache {
		//get from cache
		ridBytes, err := userSessionCache.Get(sessionid)
		if err == nil {
			return string(ridBytes), true, nil
		}
	}
	if db == nil {
		db, err = pgxdb.GetDbConn(system)
		if err != nil {
			return "", false, err
		}
		defer pgxdb.ReleaseDbConn(db)
	}

	rid, exist, err = GetUserRIDinSessionDB(sessionid, db)

	return
}

//从session表查询出用户rid
func GetUserRIDinSessionDB(sessionid string, db *pgxpool.Conn) (rid string, exist bool, err error) {
	sqlStr := "select UserRID from DbUserSession where RID = $1;"

	err = db.QueryRow(context.Background(), sqlStr, sessionid).Scan(&rid)
	if err == pgx.ErrNoRows {
		return "", false, sql.ErrNoRows
	}
	if err != nil {
		return "", false, err
	}
	return rid, true, nil
}

func PutUserInSessionCache(sid string, userRid string) {
	_ = userOrgsCache.Set(sid, []byte(userRid))
}
