package dmr

import (
	"strconv"
	"strings"
)

func padStrWithLeadingZero(str string, str_min_len int) string {
	result := str
	for len(result) < str_min_len {
		result = "0" + result
	}

	return result
}

func Dmrid2Hex(dmrid uint32) string {
	hex := strconv.FormatInt(int64(dmrid), 16)
	return padStrWithLeadingZero(strings.ToUpper(hex), 8)
}

func Hex2Dmrid(hexDmrid string) uint32 {
	id, err := strconv.ParseUint(hexDmrid, 16, 32)

	if err != nil {
		return 0
	}

	return uint32(id)
}
