module gitlab.com/kicad99/ykit/goutil

go 1.17

require (
	github.com/allegro/bigcache v1.2.1
	github.com/gogo/protobuf v1.3.2
	github.com/golang/glog v1.0.0
	github.com/google/uuid v1.3.0
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jackc/pgx/v4 v4.15.0
	github.com/nats-io/nats.go v1.13.1-0.20220121202836-972a071d373d
	github.com/nickelser/parselogical v0.0.0-20171014195826-b07373e53c91
	github.com/yangjuncode/yrpcmsg v1.2.5
	go.uber.org/atomic v1.9.0
	google.golang.org/grpc v1.44.0
)

require (
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgconn v1.11.0 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.2.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.10.0 // indirect
	github.com/jackc/puddle v1.2.1 // indirect
	github.com/nats-io/nats-server/v2 v2.7.2 // indirect
	github.com/nats-io/nkeys v0.3.0 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce // indirect
	golang.org/x/text v0.3.6 // indirect
)
