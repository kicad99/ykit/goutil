package goutil

import (
	"errors"

	nats "github.com/nats-io/nats.go"
)

//go:generate syncmap -name NatsConnMap -pkg goutil "map[string]*nats.Conn"

//sys->*nats.Conn
var allNatsConn = NatsConnMap{}

var DefaultNatsSys string

var errNoSuchSysNatsConn = errors.New("no such nats conn found")

//FindNatsConn 得到系统对应的nats conn
func FindNatsConn(sys string) (conn *nats.Conn, err error) {
	if len(sys) == 0 {
		sys = DefaultNatsSys
	}
	con, ok := allNatsConn.Load(sys)
	if ok {
		return con, nil
	} else {
		return nil, errNoSuchSysNatsConn
	}
}

func SetNatsConn(sys string, conn *nats.Conn) {
	oldConn, _ := allNatsConn.Load(sys)
	if oldConn != nil {
		oldConn.Close()
	}
	if conn != nil {
		allNatsConn.Store(sys, conn)
	}
	if len(DefaultNatsSys) == 0 {
		DefaultNatsSys = sys
	}
}

func DelNatsConn(sys string, closeOldConn bool) {
	if closeOldConn {
		SetNatsConn(sys, nil)
	} else {
		allNatsConn.Delete(sys)
	}
}
