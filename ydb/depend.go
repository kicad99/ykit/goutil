package ydb

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

//FixSQLFileExecOrder 将散乱的sql文件按照它们的依赖关系排序以方便正确执行
func FixSQLFileExecOrder(filepaths []string) (r []string, err error) {
	//sql obj name -> filepath
	provider := make(map[string]string, 0)
	//filepath -> depend sql obj name[]
	depend := make(map[string](map[string]bool), 0)

	regObj := getRegexObj()
	for _, fp := range filepaths {
		fpProvide, fpDepend, err := processOneFile(fp, regObj)
		//fmt.Println(fp, "provoide:", fpProvide, "dep:", fpDepend)
		if err != nil {
			return nil, err
		}
		depend[fp] = fpDepend
		for pro := range fpProvide {
			if val, ok := provider[pro]; ok {
				return nil, fmt.Errorf("two file provide same definition:", pro, val, fp)
			} else {
				provider[pro] = fp
			}
		}
		_, fpFile := filepath.Split(fp)
		provider[fpFile] = fp
	}

	//log.Println("provider:", provider)
	//log.Println("depend:", depend)
	//filepath -> bool
	hasInResult := make(map[string]bool, 0)
	//先排序没有依赖的文件
	for fp, depMap := range depend {
		if len(depMap) == 0 {
			r = append(r, fp)
			hasInResult[fp] = true
			delete(depend, fp)
		}
	}

	deploop := 0
	for len(depend) > 0 {
		deploop++
		if deploop > 30000 {
			for fp, deps := range depend {
				fmt.Println(fp, " dep ", deps)
			}
			return nil, fmt.Errorf("FixSQLFileExecOrder in loop > 30000,may has cycle ref")
		}
		for fp, depMap := range depend {
			isThisOk := true
			for dep := range depMap {
				profp, haspro := provider[dep]
				if !haspro {
					//没文件提供此依赖
					log.Println("sql no provide for:", dep, " dep in:", fp)
					delete(depMap, dep)
					continue
				}

				if profp == fp {
					//自己依赖自己，正常
					delete(depMap, dep)
					continue
				}

				if hasInResult[profp] {
					delete(depMap, dep)
				}

				if _, indep := depend[profp]; indep {
					//依赖项还没有添加
					isThisOk = false
					break
				}
			}
			if !isThisOk {
				continue
			}
			r = append(r, fp)
			hasInResult[fp] = true
			delete(depend, fp)
		}
	}

	return r, nil
}

func processOneFile(fp string, regObj *tsqlregex) (provide map[string]bool, depend map[string]bool, err error) {
	f, err := os.OpenFile(fp, os.O_RDONLY, 0)

	if err != nil {
		log.Println("open file error: %v", err, fp)
		return nil, nil, err
	}
	defer f.Close()

	provide = make(map[string]bool, 0)
	depend = make(map[string]bool, 0)

	rd := bufio.NewReader(f)
	for {
		line, err := rd.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}

			log.Println("read file error: %v", err, fp)
			break
		}

		lineProvider, lineDepend := processLineSqlDep(line, regObj)
		for pro := range lineProvider {
			provide[pro] = true
		}
		for dep := range lineDepend {
			depend[dep] = true
		}

		//process file dep
		m := regObj.depFile.FindStringSubmatch(line)
		if len(m) > 1 {
			depend[strings.TrimSpace(m[1])] = true
		}

	}

	for pro := range provide {
		//自己依赖自己，删除掉
		delete(depend, pro)
	}

	return

}

type tsqlregex struct {
	depalterTable    *regexp.Regexp
	depdropTrigger   *regexp.Regexp
	depSelectDbTable *regexp.Regexp
	depDel           *regexp.Regexp
	depInsert        *regexp.Regexp
	depUpdate        *regexp.Regexp
	depDefaultFn     *regexp.Regexp
	depForeignKey    *regexp.Regexp
	depTriggerFn     *regexp.Regexp
	depFile          *regexp.Regexp
	proFn            *regexp.Regexp
	proTable         *regexp.Regexp
	createIndex      *regexp.Regexp
	createTrigger    *regexp.Regexp
}

func getRegexObj() *tsqlregex {
	return &tsqlregex{
		//扫描依赖
		depalterTable:    regexp.MustCompile(`alter\s+table\s+(\w+)\s+`),
		depdropTrigger:   regexp.MustCompile(`drop\s+trigger\s+if\s+exists\s+(\w+)\s+on\s+(\w+)\s+`),
		depDel:           regexp.MustCompile(`delete\s+from\s+(\w+)\s+`),
		depSelectDbTable: regexp.MustCompile(`select\s+.+\s+from\s+(\w+)\s+where`),
		depInsert:        regexp.MustCompile(`insert\s+into\s+(\w+)\s*`),
		depUpdate:        regexp.MustCompile(`update\s+(\w+)\s+set`),
		depDefaultFn:     regexp.MustCompile(`\s+default\s+(\w+)\s*\(`),
		depForeignKey:    regexp.MustCompile(`\s+references\s+(\w+)\s*\(`),
		depTriggerFn:     regexp.MustCompile(`execute\s+procedure\s+(\w+)\s*\(`),
		depFile:          regexp.MustCompile(`--depfile\s+(\S+.sql)`),
		//扫描provide
		proFn:    regexp.MustCompile(`create\s+or\s+replace\s+function\s+(\w+)\s*\(`),
		proTable: regexp.MustCompile(`create\s+table\s+(if\s+not\s+exists)*\s*(\w+)\s*`),
		//有依赖也有provide
		//create unique index  if not exists idx_login_session_id on db_user_session_id(user_rid,login_way);
		createIndex: regexp.MustCompile(`create\s+\w*\s*index\s+(if\s+not\s+exists)*\s*(\w+)\s+on\s+(\w+)\s*\(`),
		//create trigger tri_db_device_rid_empty  before insert or update on db_device
		createTrigger: regexp.MustCompile(`create\s+trigger\s+(\w+)\s+.+on\s+(\w+)\s*`),
	}
}

func processLineSqlDep(line string, regObj *tsqlregex) (provides map[string]bool, depends map[string]bool) {
	commentPos := strings.Index(line, "--")
	if commentPos >= 0 {
		line = line[0:commentPos]
	}
	line = strings.TrimSpace(line)

	if len(line) == 0 {
		return nil, nil
	}
	line = strings.ToLower(line)
	provides = make(map[string]bool, 0)
	depends = make(map[string]bool, 0)

	m := regObj.depalterTable.FindStringSubmatch(line)
	if m != nil {
		depends[m[1]] = true
	}
	m = regObj.depDefaultFn.FindStringSubmatch(line)
	if m != nil {
		depends[m[1]] = true
	}

	m = regObj.depSelectDbTable.FindStringSubmatch(line)
	if m != nil {
		if strings.HasPrefix(m[1], "db") {
			depends[m[1]] = true
		}
	}

	m = regObj.depForeignKey.FindStringSubmatch(line)
	if m != nil {
		depends[m[1]] = true
		return
	}

	m = regObj.depdropTrigger.FindStringSubmatch(line)
	if m != nil {
		depends[m[2]] = true
		return
	}
	m = regObj.depDel.FindStringSubmatch(line)
	if m != nil {
		depends[m[1]] = true
		return
	}

	m = regObj.depInsert.FindStringSubmatch(line)
	if m != nil {
		depends[m[1]] = true
		return
	}

	m = regObj.depUpdate.FindStringSubmatch(line)
	if m != nil {
		depends[m[1]] = true
		return
	}

	m = regObj.depTriggerFn.FindStringSubmatch(line)
	if m != nil {
		depends[m[1]] = true
		return
	}

	m = regObj.proFn.FindStringSubmatch(line)
	if m != nil {
		provides[m[1]] = true
		return
	}

	m = regObj.proTable.FindStringSubmatch(line)
	if m != nil {
		provides[m[2]] = true
		return
	}

	m = regObj.createIndex.FindStringSubmatch(line)
	if m != nil {
		provides[m[2]] = true
		depends[m[3]] = true
		return
	}

	m = regObj.createTrigger.FindStringSubmatch(line)
	if m != nil {
		provides[m[1]] = true
		depends[m[2]] = true
		return
	}
	return
}
