--// 系统设置表
create table if not exists  db_sys_config  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 conf_key varchar(64) unique not null , --// 配置名
 conf_value text --// 配置的值
);

--// 表各种操作时间
--// 客户端有时需要查询后台是否已经更新了数据,可以通过此表来得到初步的信息
create table if not exists  db_table_operate_time  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 table_name varchar(64) unique , --// 数据库表名
 last_modify_time timestamp , --// 最后修改时间
 last_modify_operation int , --// 最后修改的操作 1:insert 2:update 3:delete
 last_modify_rows int --// 最后修改时影响的行数
);

--// 组织架构表
create table if not exists  db_org  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 org_self_id varchar(64) unique not null , --// 组织机构编号
 org_sort_value int default 100 , --// 排序值,由用户指定,不用管拼音或者其它语种的排序
 org_short_name varchar(32) unique not null , --// 机构名称,缩写
 org_full_name varchar(256) , --// 机构名称,全称
 note text , --// 机构详细描述
 org_is_virtual int default 2 , --// 2:真实机构,1:虚拟机构 虚拟机构没有真实设备和用户,只是引用真实机构的数据,相当于额外的分组
 dmr_id varchar(8) unique , --// DMR ID,可用作组呼的ID
 org_img uuid default '11111111-1111-1111-1111-111111111111' , --// 组织机构的图标
 parent_org_id uuid not null default '11111111-1111-1111-1111-111111111111' --// 此组织的上级机构device
);

--// 用户的一些图片数据,地图点icon等
create table if not exists  db_image  (
 rid uuid primary key , --// 行ID
 org_id uuid REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 file_name text , --// 文件名
 file_content text , --// 文件内容,经过base64编码,就是html img的dataurl
 hash text --// 文件内容的base64(sha256(file_content)
);

--// 基站列表
create table if not exists  db_base_station  (
 rid uuid primary key , --// 行ID
 org_id uuid REFERENCES db_org(rid) ON DELETE CASCADE default '00000000-0000-0000-0000-000000000000' , --// 所属的群组
 update_at timestamp not null default now_utc() , --// 最后修改时间
 self_id varchar(16) not null , --// 基站编号
 base_station_name varchar(16) not null , 
 lon double precision , --// 经度
 lat double precision --// 纬度
);

--// 控制器设备表
create table if not exists  db_controller  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE default '00000000-0000-0000-0000-000000000000' , --// 设备所属的群组
 base_station_rid text default '' , --// 所属的基站
 self_id varchar(16) not null , --// 控制器编号
 dmr_id text not null unique , --// 控制器DMR-ID
 lon double precision , --// 经度
 lat double precision , --// 纬度
 note text , --// 控制器的详细描述
 setting jsonb not null default '{}'::jsonb , --// 控制器设备的配置信息
 signal_aera jsonb not null default '{}'::jsonb , --// 控制器信号覆盖范围,point_array,polyline
 sim_info jsonb not null  default '{}'::jsonb , --// {'valid':1,'phone_number':'123456789j0','fee_start':'2016-10-01','fee_end':'2017-01-01','alert_ahead_days':7}--// 控制器sim卡信息
 room text not null default '0' , --// 默认的语音会议室
 sip_no text  unique , --// 预分配的voip电话号码
 net_time_slot int , --// 最大可用的联网时隙
 controller_type int default 0 , --// 控制器类型 0:中继 2:电话网关
 location text , --// 安装位置/地址
 can_talk boolean default false --// 中继是否带插话功能,带的话需要相同dmrid的终端存在
);

--// 控制器状态
create table if not exists  db_controller_last_info  (
 rid uuid primary key , --// 控制器ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE default '00000000-0000-0000-0000-000000000000' , --// 设备所属的群组
 last_data_time timestamp not null default '2000-01-01 00:00:00' , --// 最后上来数据的时间
 connected int default 0 --// 当前连接状态 0:offline 1:online
);

--// 控制器上线历史表,按月分表
create table if not exists  db_controller_online_history  (
 rid uuid primary key , --// 行ID
 controller_dmr_id varchar(8) , --// dmr-id
 action_time timestamp , --// satation action time
 action_code int , --// 1: register 2:disconnect refer to action_code of cc01
 note text --// action description
);

--// 电话网关黑白名单
create table if not exists  db_phone_gateway_filter  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 name text  unique not null , --// 名称
 last_modify_time timestamp , --// 最后修改时间(保存时间)
 in_black jsonb default '[]'::jsonb , --// 拨入黑名单
 in_black_enable boolean default false , --// 拨入黑名单是否启用
 in_white jsonb default '[]'::jsonb , --// 拨入白名单
 in_white_enable boolean default false , --// 拨入白名单是否启用
 out_black jsonb default '[]'::jsonb , --// 拨出黑名单
 out_black_enable boolean default false , --// 拨出黑名单是否启用
 out_white jsonb default '[]'::jsonb , --// 拨出白名单
 out_white_enable boolean default false , --// 拨出白名单是否启用
 note text , 
 setting jsonb default '{}'::jsonb --// json设置
);

--// 对讲机设备表
create table if not exists  db_device  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE default '00000000-0000-0000-0000-000000000000' , --// 设备所属的群组
 self_id varchar(16) not null unique , --// 设备名称
 dmr_id varchar(16) not null unique , --// 设备DMR-ID
 vir_orgs text default '' , --// 对讲机所属的虚拟群组,逗号分隔的群组rid
 device_user uuid default '00000000-0000-0000-0000-000000000000' , --// 对讲机所属用户,db_user中的rid
 note text default '' , --// 设备备注信息
 device_type int not null default 0 , --// 设备类型 0:对讲机手台 1：车台 3:电话网关设备 4:中继虚拟终端 5:互联网关终端 6:模拟网关终端 7:数字网关终端
 channel_last_modify_time timestamp not null default now_utc() , --// 频道数据最后修改时间
 channel jsonb not null default '{"channels": []}'::jsonb , --// 频道配置数据--// type DeviceChannel []struct {--//     No          int      `json:"No"`--//     SendGroup   string   `json:"sendGroup"`--//     ListenGroup []string `json:"listenGroup"`--// }
 priority int , --// 优先级
 gateway_filter_rid uuid REFERENCES db_phone_gateway_filter(rid) ON DELETE set null , --// 关联的电话黑白名单
 setting jsonb not null default '{}'::jsonb , --// 设备的配置信息
 last_rf_config_time timestamp not null default now_utc() , --// 写频配置最后更新时间
 last_rf_write_time timestamp not null default now_utc() --// 写频配置最后写入对讲机时间
);

--// 对讲机最后的数据信息
create table if not exists  db_device_last_info  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE default '00000000-0000-0000-0000-000000000000' , --// 设备所属的群组
 dmr_id varchar(16) not null unique , --// 设备DMR-ID
 last_data_time timestamp  default '2000-01-01 00:00:00' , --// 最后数据时间
 last_rfid_person uuid default '00000000-0000-0000-0000-000000000000' , --// 最后打卡人员的rid
 last_rfid_person_time timestamp  default '2000-01-01 00:00:00' , --// 最后读取用户身份卡时的时间
 last_rfid varchar(16) default '' , --// 最后读取的rfid卡号
 last_rfid_time timestamp default '2000-01-01 00:00:00' , --// 最后读取rfid的时间
 last_gps_time timestamp default '2000-01-01 00:00:00' , --// 最后定位的时间
 last_lon double precision default 1000 , --// 最后定位的经度
 last_lat double precision default 1000 , --// 最后定位的纬度
 device_lock_state int default 0 , --// 设备状态 0=开机；1=禁听锁机,2=禁发锁机,3=禁发禁听锁机
 ms_status text default '' , --// 对讲机最后的状态信息
 last_controller text default '' , --// 最后接收的控制器id
 last_poweron_time timestamp not null default '2000-01-01 00:00:00' , --// 设备的最后开机时间
 last_poweroff_time timestamp not null default '2000-01-01 00:00:00' , --// 设备的最后开机时间
 last_gps_invalid_time timestamp not null default '2000-01-01 00:00:00' --// 设备的最后不定位时间，上传GPS数据为V
);

--// 用户职称表
create table if not exists  db_user_title  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 title_name varchar(64) not null unique default '' , --// 职称名
 note text default '' , --// 职称备注
 title_sort_value int --// 职称排序值
);

--// 用户数据表
create table if not exists  db_user  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 用户所属的群组
 self_id varchar(16) not null unique , --// 用户自编号
 user_name varchar(32) not null default '' , --// 用户名
 user_title text default '' , --// 用户职称,可能有个,职称rid的列表,逗号分隔
 user_phone varchar(64) default '' , --// 用户电话
 user_image uuid REFERENCES db_image(rid) ON DELETE RESTRICT default '22222222-2222-2222-2222-222222222222' , --// 用户图片
 user_rfid varchar(16) default '' , --// 用户的身份卡ID
 user_login_name varchar(32) unique , --// 用户登录名
 user_login_pass text default '' , --// 用户登录密码,为base64(sha256(user_name+user_password)),系统不保存真实的密码
 user_setting jsonb not null default '{}'::jsonb , --// 用户的一些个人配置
 note text default '' --// note
);

--// 用户群组权限表
create table if not exists  db_user_privelege  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 user_rid uuid not null REFERENCES db_user(rid) ON DELETE CASCADE , --// 用户rid
 user_org uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 有权限的群组
 include_children int default 0 --// 是否包含下级群组
);

--// 用户登录的session id表
create table if not exists  db_user_session_id  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 user_rid uuid not null , --// 用户id
 login_way int not null default 1 , --// 登录渠道 1:web 2:phone 3:pc
 session_id uuid not null , --// session id
 effective_time timestamp --// 有效期
);

--// 虚拟群组信息表
create table if not exists  db_virtual_org  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 og_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 虚拟群组rid
 virtual_user uuid not null REFERENCES db_user(rid) ON DELETE CASCADE --// 群组成员rid
);

--// 用户自己的一些地图标志
create table if not exists  db_map_point  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 self_id varchar(16) not null unique , --// 编号
 point_name varchar(16) not null , --// 标志名称
 map_display_name varchar(16) not null , --// 地图上显示的名称
 note text , --// 备注
 lon double precision , --// longitude
 lat double precision , --// latitude
 start_show_level int , --// 开始显示的级别
 color_r smallint , --// 颜色R
 color_g smallint , --// 颜色G
 color_b smallint , --// 颜色B
 point_img uuid REFERENCES db_image(rid) ON DELETE RESTRICT , --// 点的图标信息
 img_or_color_point int default 0 , --// 点是颜色点=0,还是图标点=1
 marker_width int not null default 16 , --// 地图显示的大小,限制最大128
 marker_height int not null default 16 --// 地图显示的大小,限制最大128
);

--// 巡查线路点
create table if not exists  db_line_point  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 point_id varchar(16) not null , --// 编号
 point_name varchar(16) not null , --// 标志名称
 map_display_name varchar(16) not null , --// 地图上显示的名称
 note text , --// 备注
 lon double precision , --// longitude
 lat double precision , --// latitude
 start_show_level int , --// 开始显示的级别
 color_r smallint , --// 颜色R
 color_g smallint , --// 颜色G
 color_b smallint , --// 颜色B
 point_img uuid REFERENCES db_image(rid) ON DELETE RESTRICT , --// 点的图标信息
 img_or_color_point int default 0 , --// 点是颜色点,还是图标点
 point_type int default 0 , --// 巡查点类型 1无源点，2有源点,3gps虚拟点
 point_rfid varchar(16) unique , --// 巡查点的rfid,虚拟点时也要写入一个唯一值
 gps_point_radius int default 50 , --// gps虚拟点的半径,单位M
 last_lpalarm_time timestamp , --// 有源点最后低压报警时间,null为没有报警过
 last_lpalarm_state boolean default false --// 当前点是不是在低压报警状态
);

--// 巡查点最新信息
create table if not exists  db_line_point_latest_info  (
 rid uuid primary key , --// 巡查点ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 last_check_time timestamp , --// 最后巡查时间
 last_check_device_id uuid , --// 最后检查的设备rid
 last_check_user_id uuid --// 最后检查的用户rid
);

--// 巡查线路主表
create table if not exists  db_line_master  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 line_id varchar(16) not null , --// 编号
 line_name varchar(16) not null , --// 名称
 note text , --// note
 point_count int default 0 , --// 线路包含的点数量
 line_detail_modify int not null default 0 --// 此线路已经删除过点时为1,需要提示用户再确认线路是否完整,如果修改完成要设置为0
);

--// 巡查线路细表
create table if not exists  db_line_detail  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 line_id uuid REFERENCES db_line_master(rid) ON DELETE CASCADE , --// 线路的rid
 point_id uuid REFERENCES db_line_point(rid) ON DELETE CASCADE , --// 点的rid
 point_no int , --// 点在线路中的序号
 ahead_time int not null default 10 , --// 最快到达时间,单位为分钟
 delay_time int not null default 10 --// 最迟到达时间,单位为分钟
);

--// 巡查规则表
create table if not exists  db_rfid_rule_master  (
 rid uuid primary key , --// 行ID
 update_at timestamp not null default now_utc() , --// 最后修改时间
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 rule_id varchar(16) not null , --// 编号
 rule_name varchar(16) not null , --// 标志名称
 note text , --// note
 day_1 boolean default false , --// 星期一
 day_2 boolean default false , --// 星期二
 day_3 boolean default false , --// 星期三
 day_4 boolean default false , --// 星期四
 day_5 boolean default false , --// 星期五
 day_6 boolean default false , --// 星期六
 day_7 boolean default false , --// 星期七
 check_start_time time , --// 巡查开始的时间
 check_all_time int , --// 巡查一次需要的时间,单位为分钟
 check_count int not null default 1 , --// 总共需要巡查的次数
 rule_effective_type int default 0 , --// 线路巡查规则生效时间的类型 0:总是生效 1:按有效时间段
 rule_effective_start timestamp , --// 规则开始生效时间
 rule_effective_end timestamp , --// 规则生效结束时间
 rule_line_rid uuid  REFERENCES db_line_master(rid) ON DELETE SET NULL --// 巡查规则对应的线路
);

--// 开关机数据表,按月分表
create table if not exists  db_device_power_onoff  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 对讲机所属的群组
 device_id uuid REFERENCES db_device(rid) ON DELETE CASCADE , --// device rid
 user_id uuid , --// 对讲机使用人员id
 action_time timestamp not null , --// 动作时间
 action_type int --// 动作类型 0=关机；1=正常开机；2=电池开机；3=欠压复位开机
);

--// 上班下班打卡数据表,按月分表
create table if not exists  db_user_check_in_history  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 对讲机所属的群组
 device_id uuid REFERENCES db_device(rid) ON DELETE CASCADE , --// device rid
 user_id uuid , --// 对讲机使用人员id
 action_time timestamp not null , --// 读卡时间
 action_type int , --// 动作类型 1上班,2上班中打卡,3下班,
 rfid_id varchar(10) --// 身份卡的id
);

--// rfid巡查历史表,按月分表
create table if not exists  db_rfid_history  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 对讲机所属的群组
 check_time timestamp , --// 巡查时间
 checker_id uuid , --// 巡查人员rid
 device_id uuid , --// 对讲机rid
 receive_time timestamp , --// 接收时间,不一定是实时的
 receiver varchar(16) , --// 接收的控制器名称
 point_id uuid REFERENCES db_line_point(rid) ON DELETE CASCADE --// 点的标识
);

--// gps位置历史表,按月分表
create table if not exists  db_gps_history  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 对讲机所属的群组
 gps_time timestamp , --// gps time
 gps_fixed boolean , --// gps valid
 lon double precision , --// longitude
 lat double precision , --// latitude
 speed double precision , --// gps speed
 direction int , --// gps direction, north=0
 altitude int , --// gps altitude
 device_id uuid , --// device rid
 person_id uuid , --// 相关用户rid
 device_status text , --// 对讲机状态
 up_cmd varchar(12) --// 是什么命令上来的gps数据
);

--// 报警历史,要分表了,因为报警可能会非常多,客户端需要编辑此表,分表客户端处理需要特殊处理
create table if not exists  db_alarm_history  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 对讲机所属的群组
 alarm_time timestamp , --// 报警时间
 device_id uuid , --// 对讲机rid
 person_id uuid , --// 对讲机使用人员id
 dealler_rid text , --// 处理人员rid
 dealler_time timestamp , --// 处理时间
 dealler_result jsonb  not null default '{}'::jsonb , --// 处理内容
 alarm_type text --// 报警列表,数值列表，逗号分隔, 1=紧急报警；2=强行脱网报警；3=发生欠压报警；4=GPS故障报警;5=GPS遮挡--// bcxx类报警 41=入界报警;42=出界报警;51=入界回岗;52=出界离岗;61=移动监控走动提示;62=移动监控停留报警
);

--// 对讲机通话历史,按月分表
create table if not exists  db_sound_history  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 对讲机所属的群组
 device_id uuid , --// 对讲机rid
 source_info text , --// 发起者额外信息,类似电话网关打电话时的电话号码
 sound_time timestamp , --// 通话开始时间
 sound_len int default 0 , --// 对讲时长,秒
 channel int , --// 对讲所在信道
 controller varchar(16) , --// 对讲所在控制器dmr-id
 file_name text , --// 声音文件名
 person_id uuid , --// 对讲机使用人员id
 target text not null default '' , --// 通话目标dmrid
 target_info text --// 通话目标额外信息,可能是电话号码
);

--// 还没发送的命令
create table if not exists  db_not_send_cmd  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 用户/对讲机所属的群组
 schedule_time timestamp , --// 预定发送时间
 stop_time timestamp --// 截止日期,(有效期)
);

--// 已经发送的命令列表
create table if not exists  db_sent_cmd_history  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// (发起者)用户/对讲机所属的群组
 input_time timestamp , --// 命令接收的时间
 input_user_id uuid , --// 输入的用户id,可能是对讲机设备,不一定就是系统软件的用户
 sender_type int not null default 0 , --// 0:系统用户发起的命令,1:对讲机发起的命令
 schedule_time timestamp , --// 预定发送时间
 stop_time timestamp , --// 截止日期,(有效期)
 cmd_target jsonb not null default '{"group":[],"device":[],"response":{}}'::jsonb , --// 发送目标列表,dmr_id数组--// 回应状况为dmr_id:{res_time:xxxx,res_con:dmr_id}
 cmd_target_con_seq jsonb not null default '{}'::jsonb , --// 每一个发送目标对应要发送控制器列表和序号
 send_cmd text , --// 发送的命令,如CB01/CB24
 orig_cbxx jsonb not null default '{}'::jsonb , --// cbxx json
 cmd_params jsonb not null default '{}'::jsonb , --// 一些命令相关的参数
 send_time_list jsonb not null default '{}'::jsonb --// 发送时间列表
);

--// 对讲机注册信息
create table if not exists  db_device_register_info  (
 rid uuid primary key , --// 行ID
 receive_time timestamp , --// 命令接收的时间
 con_ch text , --// 接收控制器id
 dmr_id text , --// 设备id
 seller_id text , --// 经销商id
 sn text --// 注册码
);

--// 通话调度/切换信道历史表,按月分表
create table if not exists  db_call_dispatch_history  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 发起对讲机/用户所属的群组
 action_time timestamp , --// 调度时间
 person_id uuid , --// 调度人员rid,如果是中心软件发起,则此为发起用户的rid
 device_id varchar(16) , --// 发起调度的对讲机dmrid,如果是中心软件发起的则为00000000
 controller_dmrid varchar(16) , --// 接收调度命令的控制器DMR-ID,
 dispatch_target_dmrid text , --// 调度命令中的目标dmrid列表,逗号分隔,级别调度时为一个,如果是信道切换则可能有多个
 dispatch_code int , --// 调度类型bc11: 01=开启紧急信道调度	02=开启紧急基站调度	00=取消紧急信道调度,  cb21: 111=切换信道 110=取消切换信道
 dispatch_type int , --// 调度类型,参考bc11 C_TP
 target_channel int --// 目标信道
);

--// 基站调度历史
create table if not exists  db_conf_dispatch_history  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 发起对讲机/用户所属的群组
 start_time timestamp , --// 调度开始时间
 start_person_rid uuid , --// 调度人员rid,如果是中心后台发起的则为'00000000-0000-0000-0000-000000000000'
 controller_ids text , --// 被调度的控制器dmrid,逗号分隔
 conference_no text , --// 调度的会议室号
 end_org_id uuid , --// 结束会议对讲机/用户所属的群组
 end_time timestamp , --// 调度结束时间
 end_person_rid uuid , --// 结束调度人员rid,如果是中心后台发起的则为'00000000-0000-0000-0000-000000000000'
 dispatch_code int --// 调度类型--// 1:基站互联，全部通话互通--// 0：仅仅信道互联，通话目标使用发话对讲机的目标id，类似信道补点
);

--// 未确认短信表
create table if not exists  db_not_confirm_sms  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 发起对讲机/用户所属的群组
 start_time timestamp , --// 短信发起时间
 sender_dmrid text , --// 发起者dmrid
 target_dmrid text , --// 接收方dmrid
 receive_repeater text , --// 接收中继
 sms_content text , --// 短信内容
 sms_no text , --// 短信序号
 sender_user_rid text , --// 发送设备的用户rid
 note text , --// 一些其它记录,如多次发送记录
 sms_type text default '02' --// 短信类型 02:普通短信 12:重要短信
);

--// 短信历史表,短信一般很少,不分表处理了
create table if not exists  db_sms_history  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 发起对讲机/用户所属的群组
 start_time timestamp , --// 短信发起时间
 sender_dmrid text , --// 发起者dmrid
 target_dmrid text , --// 接收方dmrid
 receive_repeater text , --// 接收中继
 sms_content text , --// 短信内容
 sms_no text , --// 短信序号
 confirm_time timestamp , --// 如果是单呼短信,这个是接收方确认时间
 note text , --// 一些其它记录,如多次发送记录
 sender_user_rid text , --// 发送设备的用户rid
 sms_type text --// 短信类型 02:普通短信 12:重要短信
);

--// 频道物理数据
create table if not exists  db_ch_rf_setting  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 name text  not null , --// 名称
 rf_setting text , --// 配置参数,一些公式的可以提取出来,或者json放settings里面
 settings jsonb default '{}'::jsonb --// 辅助设置数据,Json格式
);

--// 写频配置文件
create table if not exists  db_device_setting_conf  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 conf_name text  not null , --// 名称
 last_modify_time timestamp , --// 最后修改时间(保存时间)
 user_name text , --// 保存此配置的用户名
 conf jsonb default '[]'::jsonb --// 写频
);

--// 电话网关短号
create table if not exists  db_phone_short_no  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 short_no text  unique not null , --// 名称
 last_modify_time timestamp , --// 最后修改时间(保存时间)
 ref_org_id uuid REFERENCES db_org(rid) ON DELETE set null , --// 对应的群组rid
 ref_dev_id uuid REFERENCES db_device(rid) ON DELETE set null , --// 对应的设备rid
 note text , 
 setting jsonb default '{}'::jsonb --// json设置
);

--// 电话网关使用授权
create table if not exists  db_phone_gateway_permission  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 name text , --// 名称
 last_modify_time timestamp , --// 最后修改时间(保存时间)
 perm_org_id uuid REFERENCES db_org(rid) ON DELETE CASCADE , --// 授权可以使用的群组rid
 perm_dev_id uuid REFERENCES db_device(rid) ON DELETE CASCADE , --// 授权可以使用的设备rid
 gateway_rid uuid REFERENCES db_device(rid) ON DELETE CASCADE , --// 网关rid
 note text , 
 setting jsonb default '{}'::jsonb --// json设置
);

--// 电话网关设备关系管理
create table if not exists  db_controller_gateway_manage  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 ref_controller_id uuid REFERENCES db_controller(rid) ON DELETE CASCADE , --// 电话网关控制器 rid
 phone_pos int , --// 控制器板上电话接口位置,1,2,3
 phone_no text , --// 电话号码
 ref_dev_id uuid unique REFERENCES db_device(rid) ON DELETE CASCADE --// 电话网关dev rid
);

--// 预定义电话号码本
create table if not exists  db_phone_no_list  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 所属的群组
 last_modify_time timestamp , --// 最后修改时间(保存时间)
 phone_name text  unique not null , --// 名称
 phone_no text , 
 setting jsonb default '{}'::jsonb --// json设置
);

--// 有源点报警历史
create table if not exists  db_linepoint_alarm_history  (
 rid uuid primary key , --// 行ID
 org_id uuid not null REFERENCES db_org(rid) ON DELETE CASCADE , --// 对讲机所属的群组
 check_time timestamp , --// 巡查时间
 checker_id uuid , --// 巡查人员rid
 device_id uuid , --// 对讲机rid
 receive_time timestamp , --// 接收时间,不一定是实时的
 receiver varchar(16) , --// 接收的控制器名称
 point_id uuid REFERENCES db_line_point(rid) ON DELETE CASCADE , --// 点的标识
 alarm_code int --// 报警码,低压报警为4
);

create table if not exists  db_device_channel_zone  (
 rid uuid primary key , --// 行ID
 zone_level int default 0 , --// 区域所在层级,0,1,2,3,0为最顶级(root),用户看不见
 zone_no int , --// 区域在所在层级下的序号,一个父区域下可以有多个子区域,这些子区域必须按顺序编号,从0开始
 zone_title text , --// 区域名称
 zone_parent uuid REFERENCES db_device_channel_zone(rid) ON DELETE CASCADE , --// 区域的上级,最顶级时为null
 setting jsonb not null default '{}'::jsonb --// setting
);

