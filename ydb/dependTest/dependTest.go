package main

import (
	"log"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/kicad99/ykit/goutil/ydb"
)

func main() {
	var dirFiles []string
	err := filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Fatal(err)
		}
		if !info.IsDir() && strings.HasSuffix(path, ".sql") {
			dirFiles = append(dirFiles, path)
		}
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	log.Println("all sql dirFiles:", dirFiles)

	orderSQLFiles, err := ydb.FixSQLFileExecOrder(dirFiles)

	if err != nil {
		log.Fatal(err)
	}

	log.Println("order sql files:", orderSQLFiles)
	return
}
