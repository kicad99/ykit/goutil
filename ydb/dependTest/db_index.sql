--depfile init2.sql
--sql table update
--有源点低压报警升级
ALTER TABLE db_line_point ADD COLUMN IF NOT EXISTS last_lpalarm_time timestamp NULL;
ALTER TABLE db_line_point ADD COLUMN IF NOT EXISTS last_lpalarm_state boolean default false;
ALTER TABLE db_device_channel_zone ADD COLUMN IF NOT EXISTS setting jsonb NOT NULL DEFAULT '{}';



--数据中各表的index
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

--login index
create unique index  if not exists idx_login_session_id on db_user_session_id(user_rid,login_way);
--db_image
CREATE INDEX if not exists idx_db_image_org_id ON db_image  USING btree (org_id);
CREATE INDEX if not exists idx_db_image_file_name ON db_image  USING btree (file_name);
--db_map_point
CREATE INDEX if not exists idx_db_map_point_org_id ON db_map_point  USING btree (org_id);
--db_org index
CREATE INDEX if not exists idx_db_org_parent_org_id ON db_org  USING btree (parent_org_id);
--db_device index
CREATE INDEX if not exists idx_db_device_org_id ON db_device  USING btree (org_id);
--db_device_last_info
CREATE INDEX if not exists idx_db_device_last_info_org_id ON db_device_last_info  USING btree (org_id);
--db_user
CREATE INDEX if not exists idx_db_device_org_id ON db_user  USING btree (org_id);
--db_controller
CREATE INDEX if not exists idx_db_controller_org_id ON db_controller  USING btree (org_id);
--db_controller_last_info
CREATE INDEX if not exists idx_db_controller_last_info_org_id ON db_controller_last_info  USING btree (org_id);
--alarm history index
CREATE INDEX if not exists idx_db_alarm_history_alarm_time ON db_alarm_history  USING btree (alarm_time);
CREATE INDEX if not exists idx_db_alarm_history_device_id ON db_alarm_history  USING btree (device_id);
CREATE INDEX if not exists idx_db_alarm_history_person_id ON db_alarm_history  USING btree (person_id);
--db_line_detail
CREATE INDEX if not exists idx_db_line_detail_line_id ON db_line_detail  USING btree (line_id);
--db_rfid_rule_master index
CREATE INDEX if not exists idx_db_rfid_rule_master_org_id ON db_rfid_rule_master  USING btree (org_id);
CREATE INDEX if not exists idx_db_rfid_rule_master_line_rid ON db_rfid_rule_master  USING btree (rule_line_rid);
--db_line_point
CREATE INDEX if not exists idx_db_line_point_org_id ON db_line_point  USING btree (org_id);
--db_line_point_latest_info
CREATE INDEX if not exists idx_db_line_point_latest_info_org_id ON db_line_point_latest_info  USING btree (org_id);

CREATE UNIQUE INDEX if not exists idx_db_device_register_info_sn ON db_device_register_info  USING btree (dmr_id, seller_id,sn);

CREATE UNIQUE INDEX if not exists idx_db_not_confirm_sms_unique on db_not_confirm_sms USING btree (sender_dmrid, target_dmrid, sms_no, sms_content);

CREATE UNIQUE INDEX if not exists idx_db_device_channel_zone on db_device_channel_zone using btree (zone_no,zone_parent);

--维护db_device_last_info与db_device的同步关系
drop trigger if exists tri_db_device_update on db_device CASCADE;
create or replace function fn_tri_db_device_last_info_sync() RETURNS trigger AS $$
begin

		IF (TG_OP = 'DELETE') THEN
            delete from   db_device_last_info where rid=OLD.rid;
			RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            update  db_device_last_info set(  update_at,  org_id,  dmr_id)= (now_utc(), NEW.org_id, NEW.dmr_id) where rid=NEW.rid;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO  db_device_last_info(  rid,  update_at,  org_id,  dmr_id)VALUES (  NEW.rid,  now_utc(),  NEW.org_id,  NEW.dmr_id);
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
end;
$$
LANGUAGE PLPGSQL;

CREATE TRIGGER tri_db_device_update AFTER INSERT OR UPDATE OR DELETE ON db_device
FOR EACH ROW EXECUTE PROCEDURE fn_tri_db_device_last_info_sync();

--db_device gateway_filter_rid if is empty set it as null
drop trigger if exists tri_db_device_rid_empty on db_device CASCADE;
create or replace function fn_tri_db_device_rid_empty() RETURNS trigger AS $$
begin
if new.gateway_filter_rid='00000000-0000-0000-0000-000000000000' then
	new.gateway_filter_rid=NULL;
end if;
        
RETURN NEW;
end;$$
LANGUAGE PLPGSQL;


create trigger tri_db_device_rid_empty  BEFORE INSERT OR UPDATE on db_device 
FOR EACH ROW EXECUTE PROCEDURE fn_tri_db_device_rid_empty();

--维护db_line_point_latest_info与db_line_point的同步关系
drop trigger if exists tri_db_line_point_update on db_line_point CASCADE;
create or replace function fn_tri_db_line_point_latest_info_sync() RETURNS trigger AS $$
begin

		IF (TG_OP = 'DELETE') THEN
            delete from   db_line_point_latest_info where rid=OLD.rid;
			RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            update  db_line_point_latest_info set(  update_at,  org_id)= (now_utc(), NEW.org_id) where rid=NEW.rid;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO  db_line_point_latest_info(  rid,  update_at,  org_id)VALUES (  NEW.rid,  now_utc(),  NEW.org_id);
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
end;
$$
LANGUAGE PLPGSQL;

CREATE TRIGGER tri_db_line_point_update AFTER INSERT OR UPDATE OR DELETE ON db_line_point
FOR EACH ROW EXECUTE PROCEDURE fn_tri_db_line_point_latest_info_sync();

--维护db_controller_last_info与db_controller的同步关系
drop trigger if exists tri_db_controller_update on db_controller CASCADE;
create or replace function fn_tri_db_controller_last_info_sync() RETURNS trigger AS $$
begin

		IF (TG_OP = 'DELETE') THEN
            delete from   db_controller_last_info where rid=OLD.rid;
			RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            update  db_controller_last_info set   org_id=  NEW.org_id where rid=NEW.rid;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO  db_controller_last_info(  rid,    org_id)VALUES (  NEW.rid,  NEW.org_id);
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
end;
$$
LANGUAGE PLPGSQL;

CREATE TRIGGER tri_db_controller_update AFTER INSERT OR UPDATE OR DELETE ON db_controller
FOR EACH ROW EXECUTE PROCEDURE fn_tri_db_controller_last_info_sync();

--删除用户时删除相应对讲机指定的用户
drop trigger if exists tri_db_user_device_user_sync on db_user CASCADE;
create or replace function fn_tri_db_user_device_user_sync() RETURNS trigger AS $$
begin

		IF (TG_OP = 'DELETE') THEN
			delete from db_user_session_id where user_rid=OLD.rid;
            update db_device set device_user='00000000-0000-0000-0000-000000000000' where device_user=OLD.rid;
			RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            --update  db_controller_last_info set(    org_id)= row( NEW.org_id) where rid=NEW.rid;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            --INSERT INTO  db_controller_last_info(  rid,    org_id)VALUES (  NEW.rid,  NEW.org_id);
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
end;
$$
LANGUAGE PLPGSQL;

CREATE TRIGGER tri_db_user_device_user_sync AFTER INSERT OR UPDATE OR DELETE ON db_user
FOR EACH ROW EXECUTE PROCEDURE fn_tri_db_user_device_user_sync();

--db_user user_login_name if is empty set it as null
drop trigger if exists tri_db_user_user_login_name_empty on db_user CASCADE;
create or replace function fn_db_user_user_login_name_empty() RETURNS trigger AS $$
begin
if new.user_login_name='' then
	new.user_login_name=NULL;
end if;
        
RETURN NEW;
end;$$
LANGUAGE PLPGSQL;


create trigger tri_db_user_user_login_name_empty  BEFORE INSERT OR UPDATE on db_user 
FOR EACH ROW EXECUTE PROCEDURE fn_db_user_user_login_name_empty();

--删除职位时删除相应的用户的职位
drop trigger if exists tri_db_user_title_user_title_sync on db_user_title CASCADE;
create or replace function fn_tri_db_user_title_user_title_sync() RETURNS trigger AS $$
begin

		IF (TG_OP = 'DELETE') THEN
            update db_user set user_title=replace(user_title,OLD.rid::text,'') where position(OLD.rid::text in user_title)>0;
			RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            --update  db_controller_last_info set(    org_id)= row( NEW.org_id) where rid=NEW.rid;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            --INSERT INTO  db_controller_last_info(  rid,    org_id)VALUES (  NEW.rid,  NEW.org_id);
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
end;
$$
LANGUAGE PLPGSQL;

CREATE TRIGGER tri_db_user_title_user_title_sync AFTER INSERT OR UPDATE OR DELETE ON db_user_title
FOR EACH ROW EXECUTE PROCEDURE fn_tri_db_user_title_user_title_sync();

--删除线路点时设置线路信息可能不完整
drop trigger if exists tri_db_line_detail_master_sync on db_line_detail CASCADE;
create or replace function fn_tri_db_line_detail_master_sync() RETURNS trigger AS $$
begin

		IF (TG_OP = 'DELETE') THEN
            update db_line_master set line_detail_modify=1 where rid=OLD.line_id;
			RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            --update  db_controller_last_info set(    org_id)= row( NEW.org_id) where rid=NEW.rid;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            --INSERT INTO  db_controller_last_info(  rid,    org_id)VALUES (  NEW.rid,  NEW.org_id);
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
end;
$$
LANGUAGE PLPGSQL;

CREATE TRIGGER tri_db_line_detail_master_sync BEFORE  DELETE ON db_line_detail
FOR EACH ROW EXECUTE PROCEDURE fn_tri_db_line_detail_master_sync();

--org dmr_id if is empty set it as null
drop trigger if exists tri_org_dmr_id on db_org CASCADE;
create or replace function fn_tri_org_dmr_id() RETURNS trigger AS $$
begin
if char_length(trim(new.dmr_id))=0 then
	new.dmr_id=NULL;
end if;
        
RETURN NEW;
end;$$
LANGUAGE PLPGSQL;


create trigger tri_org_dmr_id  BEFORE INSERT OR UPDATE on db_org 
FOR EACH ROW EXECUTE PROCEDURE fn_tri_org_dmr_id();

--sync db_device vir_orgs when db_org update
drop trigger if exists tri_org_device_vir_orgs_sync on db_org CASCADE;
create or replace function fn_tri_org_device_vir_orgs_sync() RETURNS trigger AS $$
begin

IF (TG_OP = 'DELETE') THEN
            
			RETURN OLD;
ELSIF (TG_OP = 'UPDATE') THEN
     -- update channels
    if (old.dmr_id <> new.dmr_id) then
      update db_device set last_rf_config_time=now_utc(), channel=fn_tri_org_del_update_device_channel(old.dmr_id,new.dmr_id, db_device.channel)  where db_device.channel @> concat('{"channels": [{"sendGroup":"' , old.dmr_id,'"}]}')::jsonb or db_device.channel @> concat('{"channels":[{"listenGroup":["' ,old.dmr_id,'"]}]}')::jsonb;
    end if;

		if (old.org_is_virtual=1 and new.org_is_virtual=2) then
		update db_device set vir_orgs=replace(vir_orgs,OLD.rid::text,'') where position(OLD.rid::text in vir_orgs)>0;
		update db_device set vir_orgs=replace(vir_orgs,',,',',') where position(',,' in vir_orgs)>0;
		end if;
            --update  db_controller_last_info set(    org_id)= ( NEW.org_id) where rid=NEW.rid;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            --INSERT INTO  db_controller_last_info(  rid,    org_id)VALUES (  NEW.rid,  NEW.org_id);
            RETURN NEW;
        END IF;
		RETURN NULL; -- result is ignored since this is an AFTER trigger

end;$$
LANGUAGE PLPGSQL;


create trigger tri_org_device_vir_orgs_sync  AFTER UPDATE on db_org 
FOR EACH ROW EXECUTE PROCEDURE fn_tri_org_device_vir_orgs_sync();


--when update or delete org should invoke this method to update device channels.
-- if newdmrid is '', just invoke delete, others invoke update.
create or replace function fn_tri_org_del_update_device_channel(olddmrid character varying, newdmrid character varying, jsonbx jsonb) RETURNS jsonb AS $$
declare
		--测试数据
		--jsonbx jsonb = '{"channels": [{"no": 1, "sendGroup": "801042D3", "listenGroup": ["801042D3", "80109943", "80109944"]}, {"no": 2, "sendGroup": "80109943", "listenGroup": ["80109943", "80109944"]}]}'::jsonb;
		newchannel jsonb = jsonbx;
		counter integer =0;
		r record;
		item jsonb;
		listenGroup jsonb;
begin
for r in (select jsonb_array_elements(jsonbx->'channels') item) loop
  		listenGroup = '[]';
  		-- whether it has olddmrid listen group.
 		if((select * from jsonb_array_elements_text(r.item->'listenGroup') where value=olddmrid) != '') then
	 		select (r.item->'listenGroup')::jsonb - olddmrid into listenGroup;

	 		if(newdmrid<>'') then
	 			select listenGroup || concat('"',newdmrid,'"')::jsonb into listenGroup;
	 		end if;

		 	select jsonb_set(newchannel, concat('{"channels", ',counter ,',"listenGroup"}')::text[], listenGroup, false) into newchannel;
 		end if;
 		if (r.item->'sendGroup' = concat('"',olddmrid, '"')::jsonb) then
 			if (newdmrid<>'') then
 				select jsonb_set(newchannel, concat('{"channels", ',counter,',"sendGroup"}')::text[], concat('"', newdmrid, '"')::jsonb, false) into newchannel;
 			else
	 			select jsonb_set(newchannel, concat('{"channels", ',counter,',"sendGroup"}')::text[], '""'::jsonb, false) into newchannel;
 			end if;
 		end if;
 		counter = counter + 1;
  end loop;
  raise notice 'new json is % \n, old is %', newchannel, jsonbx;
  return newchannel;
end;$$
LANGUAGE PLPGSQL;


--delete all sub org
drop trigger if exists tri_org_del_sub_org on db_org CASCADE;
create or replace function fn_tri_org_del_sub_org() RETURNS trigger AS $$
begin
update db_device set last_rf_config_time=now_utc(), channel=fn_tri_org_del_update_device_channel(old.dmr_id,'', db_device.channel)  where db_device.channel @> concat('{"channels": [{"sendGroup":"' , old.dmr_id,'"}]}')::jsonb or db_device.channel @> concat('{"channels":[{"listenGroup":["' ,old.dmr_id,'"]}]}')::jsonb;
update db_device set vir_orgs=replace(vir_orgs,OLD.rid::text,'') where position(OLD.rid::text in vir_orgs)>0;
update db_device set vir_orgs=replace(vir_orgs,',,',',') where position(',,' in vir_orgs)>0;
delete from db_org where parent_org_id=OLD.rid; 
RETURN OLD;
end;$$
LANGUAGE PLPGSQL;


create trigger tri_org_del_sub_org  AFTER DELETE on db_org 
FOR EACH ROW EXECUTE PROCEDURE fn_tri_org_del_sub_org();


--db_phone_short_no ref_org_id/ref_dev_id if is empty set it as null
drop trigger if exists tri_db_phone_short_no_rid_empty on db_phone_short_no CASCADE;
create or replace function fn_tri_db_phone_short_no_rid_empty() RETURNS trigger AS $$
begin
if new.ref_org_id='00000000-0000-0000-0000-000000000000' then
	new.ref_org_id=NULL;
end if;

if new.ref_dev_id='00000000-0000-0000-0000-000000000000' then
	new.ref_dev_id=NULL;
end if;
        
RETURN NEW;
end;$$
LANGUAGE PLPGSQL;


create trigger tri_db_phone_short_no_rid_empty  BEFORE INSERT OR UPDATE on db_phone_short_no 
FOR EACH ROW EXECUTE PROCEDURE fn_tri_db_phone_short_no_rid_empty();

--db_phone_gateway_permission perm_org_id/perm_dev_id if is empty set it as null
drop trigger if exists tri_db_phone_gateway_permission_rid_empty on db_phone_gateway_permission CASCADE;
create or replace function fn_tri_db_phone_gateway_permission_rid_empty() RETURNS trigger AS $$
begin
if new.perm_org_id='00000000-0000-0000-0000-000000000000' then
	new.perm_org_id=NULL;
end if;

if new.perm_dev_id='00000000-0000-0000-0000-000000000000' then
	new.perm_dev_id=NULL;
end if;
        
RETURN NEW;
end;$$
LANGUAGE PLPGSQL;


create trigger tri_db_phone_gateway_permission_rid_empty  BEFORE INSERT OR UPDATE on db_phone_gateway_permission
FOR EACH ROW EXECUTE PROCEDURE fn_tri_db_phone_gateway_permission_rid_empty();



-- fn_tri_db_controller_type_update when db_controller type update to 0;//gateway -> repeater
drop trigger if exists tri_db_controller_type_update on db_controller CASCADE;
create or replace function fn_tri_db_controller_type_update() RETURNS trigger AS $$
begin
if new.controller_type = 0 and old.controller_type = 2 then
  delete from db_controller_gateway_manage where ref_controller_id = old.rid;
end if;

RETURN NEW;
end;$$
LANGUAGE PLPGSQL;


create trigger tri_db_controller_type_update  AFTER UPDATE on db_controller
FOR EACH ROW EXECUTE PROCEDURE fn_tri_db_controller_type_update();




--db_line_point last_lpalarm_time if is empty set it as null
drop trigger if exists tri_db_line_point_last_lpalarm_time_empty on db_line_point CASCADE;
create or replace function  fn_db_line_point_last_lpalarm_time_empty() RETURNS trigger AS $$
begin
if new.last_lpalarm_time='1970-01-01' then
	new.last_lpalarm_time=NULL;
end if;
        
RETURN NEW;
end;$$
LANGUAGE PLPGSQL;

CREATE TRIGGER tri_db_line_point_last_lpalarm_time_empty BEFORE INSERT OR UPDATE  ON db_line_point
FOR EACH ROW EXECUTE PROCEDURE fn_db_line_point_last_lpalarm_time_empty();
