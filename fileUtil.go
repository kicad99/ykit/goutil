package goutil

import (
	"os"
	"path/filepath"
	"strings"
)

//ExtractFilename 提取文件名  filename.ext -> filename
func ExtractFilename(filenameWithExt string) string {
	var extension = filepath.Ext(filenameWithExt)
	return filenameWithExt[0 : len(filenameWithExt)-len(extension)]
}

//ListDirFiles 列出目录下以ext结尾的所有文件
func ListDirFiles(dir, ext string) (dirFiles []string) {
	_ = filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return nil
		}
		if !info.IsDir() && strings.HasSuffix(path, ext) {
			dirFiles = append(dirFiles, path)
		}
		return nil
	})

	return
}

// FileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
