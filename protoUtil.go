package goutil

//当proto是ygen-gogofaster(protoc-gen-gogofaster)编译出来时，会有这些接口，如果是ygen-go(protoc-gen-go)编译出来的则没有
//断言此接口可用来判断proto message 是谁编译出来的
type IgogoMarshal interface {
	Marshal() (dAtA []byte, err error)
	MarshalTo(dAtA []byte) (int, error)
	Unmarshal(dAtA []byte) error
}

//new 接口，此函数可用来生成和原来对象一样类型的新对象，用于返回
type INew interface {
	New() interface{}
}
