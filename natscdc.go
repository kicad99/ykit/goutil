package goutil

import (
	"fmt"
	"github.com/yangjuncode/yrpcmsg"
)

//通过nats 把cdc 信息发布出去
func NatsCdc(sys string, reqorggrid string, sid string, operation string, reqProto interface{}, reqOptionProto interface{}) {
	if len(reqorggrid) == 0 {
		return
	}

	natsConn, err := FindNatsConn(sys)
	if err != nil {
		fmt.Println(NowTimeStrInLocal(), "cdc can not get nats con for:", sys)
		return
	}

	var reqBytes []byte
	Ireq, ok := reqProto.(IMarshaler)
	if ok {
		reqBytes, _ = Ireq.Marshal()
	}

	var reqOptBytes []byte
	IreqOpt, ok := reqOptionProto.(IMarshaler)
	if ok {
		reqOptBytes, _ = IreqOpt.Marshal()
	}

	cdcMsg := &yrpcmsg.Ymsg{
		Cmd:    1,
		Sid:    StrToBytesUnsafe(sid),
		Optstr: operation,
		Body:   reqBytes,
		Optbin: reqOptBytes,
	}

	cdcBytes, err := cdcMsg.Marshal()
	if err != nil {
		fmt.Println("cdc marshal err:", err)
	}

	_ = natsConn.Publish("cdc."+sys+"."+reqorggrid, cdcBytes)

}
