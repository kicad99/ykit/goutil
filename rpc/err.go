package rpc

import "errors"

var ENoMetaDataFound = errors.New("No Metadata found")
var EnoDMLParamFound = errors.New("no DMLParam found")
var EDecodingErr = errors.New("decoding param err")
var EBadMetaSystemName = errors.New("Bad meta system name")
var EBadProtoMessage = errors.New("Bad proto message")
var EBadProtoParam = errors.New("Bad proto message param")
