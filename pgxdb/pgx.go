package pgxdb

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/kicad99/ykit/goutil"
	"gitlab.com/kicad99/ykit/goutil/rpc"
	"gitlab.com/kicad99/ykit/goutil/ydb"
	"google.golang.org/grpc/metadata"

	"go.uber.org/atomic"
)

var pgpool = PgxPoolMap{}

var DebugDBAcquireRelease = false

var EnoSuchSystemDbCon = errors.New("no such system db conn found")

//GetDbConn 通过系统名称获得相应的数据连接对象，用完一定要release
func GetDbConn(system string) (db *pgxpool.Conn, err error) {

	pool, exists := pgpool.Load(system)

	if exists {
		//3分钟超时，不要一直等待下去
		ctxTimeout, cancelFunc := context.WithTimeout(context.Background(), time.Minute*3)
		defer cancelFunc()
		db, err = pool.Acquire(ctxTimeout)
		if db != nil {
			if DebugDBAcquireRelease {
				beforeAcquire(nil, nil)
			}
			return db, nil
		}

		if err != nil {
			err = fmt.Errorf("GetDbConn err:%w", err)
		}

		return
	} else {
		return nil, EnoSuchSystemDbCon
	}
}

func ReleaseDbConn(db *pgxpool.Conn) {
	db.Release()
	if DebugDBAcquireRelease {
		afterRelease(nil)

	}
}

var atomAcquire atomic.Uint32
var atomRelease atomic.Uint32

func beforeAcquire(context.Context, *pgx.Conn) bool {
	acquire := atomAcquire.Inc()
	fmt.Println(goutil.NowTimeStrInLocal(), "DbAcquire=", acquire, "DbRelease=", atomRelease.Load())
	return true
}

func afterRelease(*pgx.Conn) bool {
	release := atomRelease.Inc()
	fmt.Println(goutil.NowTimeStrInLocal(), "DbRelease=", release, "DbAcquire=", atomAcquire.Load())
	return true
}

//设置数据库pool by dburl
func SetupDbConnByURL(sys string, dburl string) (db *pgxpool.Pool, err error) {
	config, err := pgxpool.ParseConfig(dburl)
	if err != nil {
		return nil, err
	}
	//config.BeforeAcquire = beforeAcquire
	//config.AfterRelease = afterRelease
	config.MaxConns = 80
	config.MaxConnLifetime = 3 * 24 * time.Hour //for release db large resource cache
	config.MaxConnIdleTime = time.Minute * 30
	config.HealthCheckPeriod = time.Minute * 5
	return SetupDbConnByConf(sys, config)
}

//设置数据库pool by config
func SetupDbConnByConf(sys string, dbconf *pgxpool.Config) (db *pgxpool.Pool, err error) {
	if dbconf.MinConns < 3 {
		dbconf.MinConns = 3
	}
	pool, err := pgxpool.ConnectConfig(context.Background(), dbconf)

	if err != nil {
		return nil, err
	}

	pgpool.Store(sys, pool)

	return pool, err
}

//InitDbByDirSQL 执行指定目录下所有的sql文件
func InitDbByDirSQL(sqlDir string, db *pgxpool.Conn) (err error) {
	sqlFiles := goutil.ListDirFiles(sqlDir, ".sql")
	orderSqlFiles, err := ydb.FixSQLFileExecOrder(sqlFiles)
	if err != nil {
		return fmt.Errorf("fix sql file order err:%w : %v", err, sqlFiles)
	}
	if len(sqlFiles) != len(orderSqlFiles) {
		log.Fatal("FixSQLFileExecOrder return less files", sqlFiles, orderSqlFiles)
	}
	for _, file := range orderSqlFiles {
		sqlContentBytes, err := ioutil.ReadFile(file)
		if err != nil {
			return fmt.Errorf("init db read sql file %s err:%w", file, err)
		}
		sqlContent := goutil.BytesToStrUnsafe(sqlContentBytes)
		_, err = db.Exec(context.Background(), sqlContent)
		if err != nil {
			return fmt.Errorf("init db sql:%s err:%w", file, err)
		}
	}

	return
}

//返回的db一定要记得释放
func GetDbFromRpcMetadata(ctx context.Context) (db *pgxpool.Conn, err error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, rpc.ENoMetaDataFound
	}
	system := md.Get("ykit-sys")
	if len(system) != 1 {
		return nil, rpc.EBadMetaSystemName
	}
	return GetDbConn(system[0])
}
func GetYkitInfoFromMetadata(ctx context.Context) (sys string, userrid string, sid string) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return
	}

	t := md.Get("ykit-sys")
	if len(t) > 0 {
		sys = t[0]
	}

	t = md.Get("ykit-sid")

	if len(t) > 0 {
		sid = t[0]
	}

	t = md.Get("ykit-userrid")
	if len(t) > 0 {
		userrid = t[0]
	}

	return
}
