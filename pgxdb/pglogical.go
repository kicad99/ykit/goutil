package pgxdb

import (
	"context"
	"fmt"
	"github.com/golang/glog"
	"github.com/jackc/pgx"
	"github.com/nickelser/parselogical"
	"gitlab.com/kicad99/ykit/goutil"
	"sync"
	"sync/atomic"
	"time"
)

//设置数据库replicationConn by dburl
func SetupTPgLogicalConnByURL(sys string, dburl string, replicationName string) (replicationObj *TPgLogicalConn, err error) {
	config, err := pgx.ParseURI(dburl)
	if err != nil {
		return nil, err
	}

	if len(replicationName) == 0 {
		replicationName = "ykit-replication"
	}

	replicationObj = &TPgLogicalConn{
		Sys:             sys,
		DBURL:           dburl,
		pgconfig:        config,
		receivedWal:     0,
		flushWal:        0,
		replicationConn: nil,
		cancel:          nil,
		sendStatusLock:  sync.Mutex{},
		slotName:        replicationName,
	}

	return replicationObj, nil

}

type TPgLogicalConn struct {
	Sys   string
	DBURL string

	pgconfig pgx.ConnConfig
	// 订阅配置
	//sub *conf.Subscribe
	// 当前 wal 位置
	receivedWal uint64
	flushWal    uint64
	// 复制连接
	replicationConn *pgx.ReplicationConn
	// 消息处理
	//handler handler.Handler
	// 取消
	cancel context.CancelFunc
	// ack 锁
	sendStatusLock sync.Mutex

	slotName string
}

func (s *TPgLogicalConn) getReceivedWal() uint64 {
	return atomic.LoadUint64(&s.receivedWal)
}

func (s *TPgLogicalConn) setReceivedWal(val uint64) {
	atomic.StoreUint64(&s.receivedWal, val)
}

//func (s *TPgLogicalConn) getFlushWal() uint64 {
//	return atomic.LoadUint64(&s.flushWal)
//}
//
//func (s *TPgLogicalConn) setFlushWal(val uint64) {
//	atomic.StoreUint64(&s.flushWal, val)
//}

func (s *TPgLogicalConn) getStatus() (*pgx.StandbyStatus, error) {
	return pgx.NewStandbyStatus(s.getReceivedWal())
}

func (s *TPgLogicalConn) start(ctx context.Context) error {

	glog.Infof("start TPgLogicalConn for %s", s.slotName)
	ctx, s.cancel = context.WithCancel(ctx)

	config := pgx.ConnConfig{Host: s.pgconfig.Host, Port: s.pgconfig.Port, Database: s.pgconfig.Database, User: s.pgconfig.User, Password: s.pgconfig.Password}
	conn, err := pgx.ReplicationConnect(config)
	if err != nil {
		glog.Errorf("create replication connection err: %v", err)
		return err
	}

	s.replicationConn = conn

	slotname := s.slotName

	_, _, err = conn.CreateReplicationSlotEx(slotname, "test_decoding")
	if err != nil {
		// 42710 means replication slot already exists
		if pgerr, ok := err.(pgx.PgError); !ok || pgerr.Code != "42710" {
			glog.Errorf("create replication slot err: %v", err)
			return fmt.Errorf("failed to create replication slot: %s", err)
		}
	}

	_ = s.sendStatus()

	// Handle old data from db
	//if err := s.exportSnapshot(snapshotID); err != nil {
	//	glog.Errorf("export snapshot %s err: %v", snapshotID, err)
	//	return fmt.Errorf("slot name %s, err export snapshot: %v", s.slotName, err)
	//}

	if err := conn.StartReplication(slotname, 0, -1); err != nil {
		glog.Errorf("start replication err: %v", err)
		return err
	}

	return s.runloop(ctx)
}

func (s *TPgLogicalConn) stop() error {
	s.cancel()
	//s.handler.Stop()
	return s.replicationConn.Close()
}

func (s *TPgLogicalConn) runloop(ctx context.Context) error {
	defer s.stop()

	go func() {
		ticker := time.NewTicker(5 * time.Second)
		for {
			select {
			case <-ticker.C:
				_ = s.sendStatus()
			case <-ctx.Done():
				return
			}
		}
	}()

	for {
		msg, err := s.replicationConn.WaitForReplicationMessage(ctx)
		if err != nil {
			if err == ctx.Err() {
				return err
			}
			if err := s.checkAndResetConn(); err != nil {
				glog.Errorf("reset replication connection err: %v", err)
			}
			continue
		}

		if msg == nil {
			continue
		}

		if err := s.replicationMsgHandle(msg); err != nil {
			glog.Errorf("handle replication msg err: %v", err)
			continue
		}
	}
}

func (s *TPgLogicalConn) checkAndResetConn() error {
	if s.replicationConn != nil && s.replicationConn.IsAlive() {
		return nil
	}

	time.Sleep(time.Second * 10)

	config := pgx.ConnConfig{
		Host:     s.pgconfig.Host,
		Port:     s.pgconfig.Port,
		Database: s.pgconfig.Database,
		User:     s.pgconfig.User,
		Password: s.pgconfig.Password,
	}
	conn, err := pgx.ReplicationConnect(config)
	if err != nil {
		return err
	}

	if _, _, err := conn.CreateReplicationSlotEx(s.slotName, "test_decoding"); err != nil {
		if pgerr, ok := err.(pgx.PgError); !ok || pgerr.Code != "42710" {
			return fmt.Errorf("failed to create replication slot: %s", err)
		}
	}

	if err := conn.StartReplication(s.slotName, 0, -1); err != nil {
		_ = conn.Close()
		return err
	}

	s.replicationConn = conn

	return nil
}

// ReplicationMsgHandle handle replication msg
func (s *TPgLogicalConn) replicationMsgHandle(msg *pgx.ReplicationMessage) error {

	// 回复心跳
	if msg.ServerHeartbeat != nil {

		if msg.ServerHeartbeat.ServerWalEnd > s.getReceivedWal() {
			s.setReceivedWal(msg.ServerHeartbeat.ServerWalEnd)
		}
		if msg.ServerHeartbeat.ReplyRequested == 1 {
			_ = s.sendStatus()
		}
	}

	if msg.WalMessage != nil {

		result := parselogical.NewParseResult(goutil.BytesToStrUnsafe(msg.WalMessage.WalData))
		if err := result.Parse(); err != nil {
			return fmt.Errorf("invalid pgoutput msg: %s", err)
		}
		//
		//logmsg, err := model.Parse(msg.WalMessage)
		//if err != nil {
		//	return fmt.Errorf("invalid pgoutput msg: %s", err)
		//}
		//
		//logmsg.Timestamp = time.Now().UnixNano() / int64(time.Millisecond)
		if err := s.handleWalMessage(result); err != nil {
			return err
		}
	}

	return nil
}
func (s *TPgLogicalConn) handleWalMessage(walData *parselogical.ParseResult) (err error) {
	return nil
}

//func (s *TPgLogicalConn) handleMessage(data *model.WalData) (err error) {
//	glog.Infof("handle wal data: %v", data)
//	var needFlush bool
//	switch data.OperationType {
//
//	// 事务开始
//	case model.Begin:
//	// 	事务结束
//	case model.Commit:
//		needFlush = true
//	default:
//		//s.datas = append(s.datas, data)
//		// 防止大事务耗尽内存
//		//needFlush = len(s.datas) > 1000
//	}
//
//	if needFlush {
//		//_ = s.flush()
//	}
//
//	return nil
//}

// 发送心跳
func (s *TPgLogicalConn) sendStatus() error {
	s.sendStatusLock.Lock()
	defer s.sendStatusLock.Unlock()

	glog.Info("pglogical send heartbeat")
	status, err := s.getStatus()
	if err != nil {
		return err
	}
	return s.replicationConn.SendStandbyStatus(status)
}
