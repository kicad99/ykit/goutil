package crudlog

import (
	"context"
	"fmt"
	"gitlab.com/kicad99/ykit/goutil"
	"gitlab.com/kicad99/ykit/goutil/crud"
	"gitlab.com/kicad99/ykit/goutil/pgxdb"
)

var GlobalCrudLogFN TfnCrudLog

type TfnCrudLog func(sys string, userrid string, operation string, reqProto interface{}, reqOptionProto interface{}, err error)

func CrudLog(sys string, userrid string, operation string, req interface{}, reqOption interface{}, err error) {
	if GlobalCrudLogFN != nil {
		GlobalCrudLogFN(sys, userrid, operation, req, reqOption, err)
	}
}

func CrudLogCtx(ctx context.Context, operation string, req interface{}, err error) {
	if GlobalCrudLogFN != nil {
		sys, userrid, _ := pgxdb.GetYkitInfoFromMetadata(ctx)
		param, _ := crud.GetDMLParamFromRpcMetadata(ctx)
		GlobalCrudLogFN(sys, userrid, operation, req, param, err)
	}
}

func CrudLogStdout(sys string, userrid string, operation string, reqProto interface{}, reqOptionProto interface{}, err error) {
	fmt.Println(goutil.NowTimeStrInUtc(), " sys:", sys, " userrid:", userrid, " op:", operation, " req:", reqProto, " reqOpt:", reqOptionProto, " err:", err)
}
